package com.sword.signature.tezos.service.impl

import com.sword.signature.merkletree.utils.byteArrayToHexString
import com.sword.signature.merkletree.utils.hexStringToByteArray
import com.sword.signature.tezos.service.TezosUtilService
import org.bouncycastle.crypto.Signer
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters
import org.bouncycastle.crypto.signers.Ed25519Signer
import org.ej4tezos.crypto.impl.TezosCryptoProviderImpl
import org.ej4tezos.model.TezosConstants
import org.ej4tezos.model.TezosIdentity
import org.ej4tezos.model.TezosPublicKey
import org.ej4tezos.papi.TezosCryptoProvider
import org.ej4tezos.papi.model.Ed25519KeyPair
import org.ej4tezos.utils.bytes.ByteToolbox
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets
import java.security.SecureRandom

@Service
class TezosUtilServiceImpl() : TezosUtilService {

    private val tezosCryptoProvider: TezosCryptoProvider

    init {
        tezosCryptoProvider = TezosCryptoProviderImpl().apply {
            setSecureRandom(SecureRandom())
            init()
        }
    }

    override fun retrieveIdentity(publicKeyBase58: String, secretKeyBase58: String): TezosIdentity {
        LOGGER.debug("retrieveIdentity (publicKey = {}, secretKey = {})", publicKeyBase58, secretKeyBase58)

        // Retrieve the key pair:
        // Note that extraRawPublicKey is used to extract secret key as well due to key length consideration.
        val publicKeyBytes: ByteArray =
            tezosCryptoProvider.extractRawPublicKey(TezosPublicKey.toTezosPublicKey(publicKeyBase58))
        val secretKeyBytes: ByteArray =
            tezosCryptoProvider.extractRawPublicKey(TezosPublicKey.toTezosPublicKey(secretKeyBase58))
        val keyPair = Ed25519KeyPair.toEd25519KeyPair(publicKeyBytes, secretKeyBytes)

        // Instantiate the TezosIdentity with the right formatted keys and address:
        val result = retrieveIdentity(keyPair)

        // Log and return the result:
        LOGGER.debug("identify retrieved: {}", result)
        return result
    }

    override fun unpackSignerAddress(packedAddress: String): String {
        LOGGER.debug("Unpacking signer address '$packedAddress'")
        // Remove the encoded prefix
        val addressWithoutPrefix = ByteToolbox.mid(hexStringToByteArray(packedAddress), 2, 20)

        // Prepend the prefix
        val prependedData = ByteToolbox.join(TezosConstants.TZ1_PREFIX, addressWithoutPrefix)

        // Add the checksum
        val checksum = tezosCryptoProvider.computeDoubleCheckSum(prependedData)
        val prependedDataWithChecksum = ByteToolbox.join(prependedData, checksum)

        return tezosCryptoProvider.encode(prependedDataWithChecksum, TezosCryptoProvider.EncoderType.BASE58)
    }

    override fun computeScriptExpr(packedData: ByteArray): String {
        val digestedData = tezosCryptoProvider.blake2B256Digester.digest(packedData)

        val prependedData = ByteToolbox.join(hexStringToByteArray("0d2c401b"), digestedData)

        val checksum = tezosCryptoProvider.computeDoubleCheckSum(prependedData)
        val prependedDataWithChecksum = ByteToolbox.join(prependedData, checksum)
        return tezosCryptoProvider.encode(prependedDataWithChecksum, TezosCryptoProvider.EncoderType.BASE58)
    }

    override fun checkKeyPairValidity(publicKeyBase58: String, secretKeyBase58: String): Boolean {
        val publicKeyBytes: ByteArray =
            tezosCryptoProvider.extractRawPublicKey(TezosPublicKey.toTezosPublicKey(publicKeyBase58))
        val secretKeyBytes: ByteArray =
            tezosCryptoProvider.extractRawPublicKey(TezosPublicKey.toTezosPublicKey(secretKeyBase58))

        val publicKey = Ed25519PublicKeyParameters(publicKeyBytes, 0)
        val privateKey = Ed25519PrivateKeyParameters(secretKeyBytes, 0)

        // the message
        val message = "Message to sign".toByteArray(StandardCharsets.UTF_8)

        // create the signature
        val signer: Signer = Ed25519Signer()
        signer.init(true, privateKey)
        signer.update(message, 0, message.size)
        val signature = signer.generateSignature()

        // verify the signature
        val verifier: Signer = Ed25519Signer()
        verifier.init(false, publicKey)
        verifier.update(message, 0, message.size)

        return verifier.verifySignature(signature)
    }

    override fun retrieveIdentity(keyPair: Ed25519KeyPair): TezosIdentity {
        // Format the public key:
        val tezosPublicKeyRaw =
            ByteToolbox.join(TezosConstants.EDPK_PREFIX, keyPair.publicKey)
        val tezosPublicKeyRawChecksum =
            tezosCryptoProvider.computeDoubleCheckSum(tezosPublicKeyRaw)
        val tezosCheckedPublicKeyRaw =
            ByteToolbox.join(tezosPublicKeyRaw, tezosPublicKeyRawChecksum)
        val tezosPublicKey =
            tezosCryptoProvider.encode(tezosCheckedPublicKeyRaw, TezosCryptoProvider.EncoderType.BASE58)

        // Format the private key:
        val tezosPrivateKeyRaw =
            ByteToolbox.join(TezosConstants.EDSK_PREFIX, keyPair.privateKey, keyPair.publicKey)
        val tezosPrivateKeyRawChecksum =
            tezosCryptoProvider.computeDoubleCheckSum(tezosPrivateKeyRaw)
        val tezosCheckedPrivateKeyRaw =
            ByteToolbox.join(tezosPrivateKeyRaw, tezosPrivateKeyRawChecksum)
        val tezosPrivateKey =
            tezosCryptoProvider.encode(tezosCheckedPrivateKeyRaw, TezosCryptoProvider.EncoderType.BASE58)

        // Format the public address:
        val digestedPublicKey =
            tezosCryptoProvider.blake2B160Digester.digest(keyPair.publicKey)
        val tezosPublicAddressRaw = ByteToolbox.join(TezosConstants.TZ1_PREFIX, digestedPublicKey)
        val tezosPublicAddressRawChecksum =
            tezosCryptoProvider.computeDoubleCheckSum(tezosPublicAddressRaw)
        val tezosPublicAddress = tezosCryptoProvider.encode(
            ByteToolbox.join(tezosPublicAddressRaw, tezosPublicAddressRawChecksum),
            TezosCryptoProvider.EncoderType.BASE58
        )

        // Instantiate the TezosIdentity with the right formatted keys and address:
        return TezosIdentity.toTezosIdentity(tezosPrivateKey, tezosPublicKey, tezosPublicAddress)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TezosUtilServiceImpl::class.java)
    }

}
