package com.sword.signature.tezos.node.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import org.slf4j.LoggerFactory
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneOffset

data class TzOperationResponse(
    val hash: String,
    val destination: String,
    val rootHash: String,
    val timestamp: OffsetDateTime,
    val packedSignerAddress: String,
)

data class TzOperationResponseAPI(
    @JsonProperty("hash")
    val hash: String,
    @JsonProperty("contents")
    val contents: JsonNode?
) {

    fun toReadableObject(): TzOperationResponse? {
        try {
            if (this.contents == null) {
                LOGGER.debug("Not a signature transaction")
                return null
            }

            val diff = this.contents[0]?.get("metadata")?.get("operation_result")?.get("big_map_diff")?.get(0)

            val destination = this.contents[0]?.get("destination")?.textValue() ?: return null

            val rootHash = diff?.get("key")?.get("string")?.textValue() ?: return null

            val epochTime =
                diff.get("value")?.get("args")?.get(0)?.get("int")?.textValue() ?: return null

            val timestamp = OffsetDateTime.ofInstant(
                Instant.ofEpochMilli(epochTime.toLong() * 1000),
                ZoneOffset.UTC
            ) ?: return null

            val packedSignerAddress =
                diff.get("value").get("args")[1]?.get("bytes")?.textValue() ?: return null

            return TzOperationResponse(
                hash = this.hash,
                destination = destination,
                rootHash = rootHash,
                timestamp = timestamp,
                packedSignerAddress = packedSignerAddress
            )
        } catch (e: Exception) {
            LOGGER.error(e.message)
            return null
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TzOperationResponse::class.java)
    }
}
