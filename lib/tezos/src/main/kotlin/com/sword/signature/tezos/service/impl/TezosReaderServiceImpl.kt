package com.sword.signature.tezos.service.impl

import com.sword.signature.merkletree.utils.hexStringToByteArray
import com.sword.signature.tezos.node.NodeConnector
import com.sword.signature.tezos.node.model.StorageResponse
import com.sword.signature.tezos.node.model.TzOperationResponse
import com.sword.signature.tezos.service.TezosReaderService
import com.sword.signature.tezos.service.TezosUtilService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class TezosReaderServiceImpl(
    val nodeConnector: NodeConnector,
    val tezosUtilService: TezosUtilService
) : TezosReaderService {

    override suspend fun hashAlreadyExists(contractAddress: String, hash: String): Boolean {
        val response = getValueFromContractStorage(contractAddress, hash)
        return response != null
    }

    override suspend fun getValueFromContractStorage(contractAddress: String, rootHash: String): StorageResponse? {
        if (contractAddress.isBlank() || rootHash.isBlank()) throw IllegalArgumentException("Arguments should not be blank.")

        val storageId = nodeConnector.getStorageId(contractAddress) ?: return null
        val scriptExpr = tezosUtilService.computeScriptExpr(packData(rootHash))
        return nodeConnector.getValueFromStorage(storageId, scriptExpr)
    }

    override suspend fun getContractStorageId(contractAddress: String): String? {
        return nodeConnector.getStorageId(contractAddress)
    }

    override suspend fun getTezosAccountBalance(publicAddress: String): Long {
        return nodeConnector.getBalance(publicAddress)
    }

    override suspend fun getTransaction(
        contractAddress: String,
        blockHash: String,
        transactionHash: String
    ): TzOperationResponse? {
        LOGGER.debug("Getting transaction '$transactionHash' on block '$blockHash', with contract '$contractAddress'")
        return nodeConnector.getBlockOperations(contractAddress, blockHash)
            .find {
                it.hash == transactionHash
            }
    }

    override suspend fun getBlockDepth(blockHash: String): Long? {
        LOGGER.debug("Getting block '$blockHash' depth")
        val headLevel = nodeConnector.getHeadBlockLevel()
        val blockLevel = nodeConnector.getBlockLevel(blockHash) ?: return null
        return headLevel - blockLevel
    }

    override suspend fun packData(hash: String): ByteArray {
        return hexStringToByteArray(nodeConnector.packData(hash).packedData)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TezosReaderServiceImpl::class.java)
    }

}
