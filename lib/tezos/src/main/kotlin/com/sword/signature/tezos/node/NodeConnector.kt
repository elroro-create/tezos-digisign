package com.sword.signature.tezos.node

import com.sword.signature.tezos.node.exception.NodeException
import com.sword.signature.tezos.node.model.*
import org.apache.http.HttpHeaders
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitExchange

@Component
class NodeConnector(
    private val nodeWebClient: WebClient
) {

    /**
     * Gets balance from given public address, 0L if the account don't exist on the blockchain.
     * @param tezosPublicAddress Public address of the account to check
     */
    suspend fun getBalance(tezosPublicAddress: String): Long {
        return genericGet("/chains/main/blocks/head/context/contracts/$tezosPublicAddress/balance") ?: 0L
    }

    /**
     * Gets level of given block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlockLevel(blockHash: String, predecessorOffset: Long = 0L): Long? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet<TzBlockLevelResponse>("/chains/main/blocks/$blockId/helpers/current_level")?.level
    }

    suspend fun getHeadBlockLevel(): Long {
        return getBlockLevel(HEAD_BLOCK)!! // Head block always exists
    }

    /**
     * Gets hashes of the operations on a given block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlockOperationHashes(blockHash: String, predecessorOffset: Long = 0L): List<String>? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet("/chains/main/blocks/$blockId/operation_hashes/3")
    }

    suspend fun getHeadBlockOperationHashes(): List<String> {
        return getBlockOperationHashes(HEAD_BLOCK)!! // Head block always exists
    }

    /**
     * Gets hash of the given block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlockHash(blockHash: String, predecessorOffset: Long = 0L): String? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet<String>("/chains/main/blocks/$blockId/hash")?.trim()?.trim('"')
    }

    suspend fun getHeadBlockHash(): String {
        return getBlockHash(HEAD_BLOCK)!! // Head block always exists
    }

    /**
     * Gets the block, null if the block is not on the blockchain
     * @param blockHash Hash of the block
     * @param predecessorOffset Offset to get a predecessor of the given blockHash
     */
    suspend fun getBlock(blockHash: String, predecessorOffset: Long = 0L): TzBlockResponse? {
        val blockId = getBlockId(blockHash, predecessorOffset)
        return genericGet("/chains/main/blocks/$blockId")
    }

    suspend fun getHeadBlock(): TzBlockResponse {
        return getBlock(HEAD_BLOCK)!! // Head block always exists
    }

    /**
     * Gets operations on a given block, empty list if the block is not on the blockchain
     * @param contractAddress Smart contract concerned by the operation
     * @param blockHash Hash of the block
     */
    @Suppress("UNCHECKED_CAST")
    suspend fun getBlockOperations(contractAddress: String, blockHash: String): List<TzOperationResponse> {
        val list = genericGet<List<TzOperationResponseAPI>>("/chains/main/blocks/$blockHash/operations/3")
            ?.map {
                it.toReadableObject()
            }?.filter {
                it != null && it.destination == contractAddress // Removes all null values.
            }

        return if (list == null) {
            emptyList()
        } else {
            list as List<TzOperationResponse>
        }
    }

    suspend fun getValueFromStorage(storageId: String, scriptExpr: String): StorageResponse? {
        if (storageId.isBlank() || scriptExpr.isBlank()) return null
        return genericGet("/chains/main/blocks/head/context/big_maps/$storageId/$scriptExpr")
    }

    suspend fun getStorageId(contractAddress: String): String? {
        return genericGet<ContractStorageIdResponse>("/chains/main/blocks/head/context/contracts/$contractAddress/storage")?.int
    }

    suspend fun packData(hash: String): PackedDataResponse {
        return nodeWebClient.post()
            .uri("/chains/main/blocks/head/helpers/scripts/pack_data")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .body(
                BodyInserters.fromValue(
                    PackedDataRequest(
                        data = PackedDataRequest.Element(hash)
                    )
                )
            )
            .awaitExchange {
                when {
                    it.statusCode().is2xxSuccessful -> it.awaitBody()
                    else -> throw NodeException.NodeAccessException()
                }
            }
    }

    private suspend inline fun <reified T : Any> genericGet(uri: String): T? {
        return try {
            nodeWebClient.get()
                .uri(uri)
                .awaitExchange {
                    when {
                        it.statusCode().is2xxSuccessful -> {
                            try {
                                it.awaitBody()
                            } catch (e: Exception) {
                                e.printStackTrace()
                                throw e
                            }
                        }
                        it.statusCode().is4xxClientError -> throw NodeException.NotFound()
                        else -> throw NodeException.NodeAccessException()
                    }
                }
        } catch (notFoundException: NodeException.NotFound) {
            return null
        } catch (e: Exception) {
            LOGGER.error(e.message)
            null
        }
    }

    private fun getBlockId(blockHash: String, predecessorOffset: Long): String {
        return if (predecessorOffset == 0L) {
            blockHash
        } else {
            "$blockHash~$predecessorOffset"
        }
    }

    companion object {
        const val HEAD_BLOCK = "head"
        private val LOGGER = LoggerFactory.getLogger(NodeConnector::class.java)
    }
}
