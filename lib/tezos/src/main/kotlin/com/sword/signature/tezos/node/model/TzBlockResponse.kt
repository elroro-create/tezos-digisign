package com.sword.signature.tezos.node.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime

data class TzBlockResponse(
    @JsonProperty("header")
    val header: TzBlockHeader,
    @JsonProperty("hash")
    val hash: String
) {
    data class TzBlockHeader(
        @JsonProperty("level")
        val level: Long,
        @JsonProperty("timestamp")
        val timestamp: OffsetDateTime
    )
}
