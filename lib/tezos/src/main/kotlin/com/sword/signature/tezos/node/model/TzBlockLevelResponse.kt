package com.sword.signature.tezos.node.model

import com.fasterxml.jackson.annotation.JsonProperty

data class TzBlockLevelResponse(
    @JsonProperty("level")
    val level: Long,
)
