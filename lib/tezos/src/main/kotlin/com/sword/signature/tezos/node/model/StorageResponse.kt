package com.sword.signature.tezos.node.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import java.time.OffsetDateTime

class StorageResponse() {

    lateinit var timestamp: OffsetDateTime

    lateinit var signerAddress: String

    constructor(timestamp: OffsetDateTime, signerAddress: String) : this() {
        this.timestamp = timestamp
        this.signerAddress = signerAddress
    }

    @JsonProperty("args")
    private fun unpackNested(args: JsonNode) {
        timestamp = OffsetDateTime.parse(args[0].get("string").textValue())
        signerAddress = args[1].get("string").textValue()
    }

}
