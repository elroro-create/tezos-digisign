package com.sword.signature.tezos.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class TezosWriterConfiguration {

    @Bean
    fun nodeWebClient(webClientBuilder: WebClient.Builder, @Value("\${tezos.node.url}") nodeUrl: String): WebClient {
        return webClientBuilder
            .baseUrl(nodeUrl)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .exchangeStrategies(
                ExchangeStrategies.builder()
                    .codecs {
                        it
                            .defaultCodecs()
                            .maxInMemorySize(16 * 1024 * 1024)
                    }
                    .build()
            )
            .build()
    }
}
