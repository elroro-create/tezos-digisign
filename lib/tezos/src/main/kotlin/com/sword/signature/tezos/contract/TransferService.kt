package com.sword.signature.tezos.contract

import org.ej4tezos.impl.TezosAbstractServiceSword
import org.ej4tezos.model.TezosIdentity
import org.slf4j.LoggerFactory

class TransferService : TezosAbstractServiceSword() {

    private val transferRunOperationTemplate by lazy { loadTemplate("operations/transfer/transfer_runOperation.json") }
    private val transferForgeOperationTemplate by lazy { loadTemplate("operations/transfer/transfer_forgeOperation.json") }
    private val transferPreApplyOperationTemplate by lazy { loadTemplate("operations/transfer/transfer_preApplyOperation.json") }

    /**
     * Transfer Tz from an account to another
     * @param from the account debited
     * @param to    the public address of the credited account
     * @param amount the amount of Tz transferred (in micro tez, 1 tez = 1 000 000 micro tez)
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    fun transfer(from: TezosIdentity, to: String, amount: Long): String {
        // Set the source of the transfer
        this.apply {
            setAdminPrivateKey(from.privateKey)
            init()
        }

        val parameters = mapOf("accountToCredit" to to, "amount" to amount.toString())

        val invokeResult =
            invoke(
                transferRunOperationTemplate,
                transferForgeOperationTemplate,
                transferPreApplyOperationTemplate,
                parameters
            )

        val transactionHash = invokeResult.transactionHash.toString()
        LOGGER.info(
            "transfer result (transactionHash = {})",
            transactionHash
        )

        return transactionHash
    }


    companion object {
        private val LOGGER = LoggerFactory.getLogger(TransferService::class.java)
    }
}
