package com.sword.signature.tezos.service

import org.ej4tezos.api.exception.TezosException
import org.ej4tezos.model.TezosIdentity

interface TezosWriterService {

    /**
     * Anchor a root hash within the Tezos blockchain.
     * @param rootHash Root hash to anchor.
     * @param signerIdentity Crypto identity of the signer.
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    @Throws(TezosException::class)
    fun anchorHash(rootHash: String, signerIdentity: TezosIdentity): String

    /**
     * Generate a new key pair, the generated key pair is associated
     * with an empty account that has not yet been revealed.
     * @return Crypto identity of the new account.
     */
    @Throws(TezosException::class)
    fun generateNewIdentity(): TezosIdentity

    /**
     * Generate a new key pair, the generated key pair is associated
     * with an empty account that has not yet been revealed.
     * @param from Crypto identity of debited account.
     * @param to Public address of the credited account.
     * @param amount The amount transferred.
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    @Throws(TezosException::class)
    fun transferTezos(from: TezosIdentity, to: String, amount: Long): String

    /**
     * Reveal an account
     * @param identity Crypto identity to reveal
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    fun revealIdentity(identity: TezosIdentity): String

}
