package com.sword.signature.tezos.node.model

import com.fasterxml.jackson.annotation.JsonProperty

data class ContractStorageIdResponse(
    @JsonProperty("int")
    val int: String
)
