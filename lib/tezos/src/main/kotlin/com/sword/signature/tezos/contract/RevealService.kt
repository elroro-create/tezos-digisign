package com.sword.signature.tezos.contract

import org.ej4tezos.impl.TezosAbstractServiceSword
import org.ej4tezos.model.TezosIdentity
import org.slf4j.LoggerFactory

class RevealService : TezosAbstractServiceSword() {

    private val revealRunOperationTemplate by lazy { loadTemplate("operations/reveal/reveal_runOperation.json") }
    private val revealForgeOperationTemplate by lazy { loadTemplate("operations/reveal/reveal_forgeOperation.json") }
    private val revealPreApplyOperationTemplate by lazy { loadTemplate("operations/reveal/reveal_preApplyOperation.json") }

    /**
     * Reveals an account
     * @param identity the account debited
     * @return The transactionId of the operation within the Tezos blockchain.
     */
    fun reveal(identity: TezosIdentity): String {
        LOGGER.debug("reveal call (identity = {}", identity)

        // Set the source of the reveal
        this.apply {
            setAdminPrivateKey(identity.privateKey)
            init()
        }

        val parameters = mapOf("publicKey" to identity.publicKey.value)

        val invokeResult =
            invoke(
                revealRunOperationTemplate,
                revealForgeOperationTemplate,
                revealPreApplyOperationTemplate,
                parameters
            )

        val transactionHash = invokeResult.transactionHash.toString()
        LOGGER.info(
            "reveal result (transactionHash = {})",
            transactionHash
        )

        return transactionHash
    }


    companion object {
        private val LOGGER = LoggerFactory.getLogger(RevealService::class.java)
    }
}
