package com.sword.signature.tezos.service

import com.sword.signature.tezos.node.model.StorageResponse
import com.sword.signature.tezos.node.model.TzOperationResponse

interface TezosReaderService {

    /**
     * Check if the provided hash is already in the contract storage.
     * @param contractAddress Address of the smart contract which storage may contain the provided hash.
     * @param hash Hash to check existence.
     * @return Existence of the provided hash in the given contract storage.
     */
    suspend fun hashAlreadyExists(contractAddress: String, hash: String): Boolean

    /**
     * Get the storage value of a given rootHash
     * @param contractAddress Address of the smart contract which storage may contain the provided hash.
     * @param rootHash Hash to get information.
     * @return Existence of the provided hash in the given contract storage.
     */
    suspend fun getValueFromContractStorage(contractAddress: String, rootHash: String): StorageResponse?

    /**
     * Get the storage id of a smart contract
     * @param contractAddress Address of the smart contract
     */
    suspend fun getContractStorageId(contractAddress: String): String?

    /**
     * Retrieve the balance of a given account
     * @param publicAddress hash of the account
     * @return the balance of the given account, will be 0 if the account doesn't exist
     */
    suspend fun getTezosAccountBalance(publicAddress: String): Long

    /**
     * Find an operation on the blockchain with the blockHash of the block on which it was anchored
     * @param contractAddress Address of the smart contract related to the operation.
     * @param blockHash Hash of the block on which the operation was anchored
     * @param transactionHash Hash of the transaction to find
     */
    suspend fun getTransaction(
        contractAddress: String,
        blockHash: String,
        transactionHash: String
    ): TzOperationResponse?

    /**
     * Get the depth of a given block, null if the block don't exist
     * @param blockHash Hash of the block on which the operation was anchored
     */
    suspend fun getBlockDepth(blockHash: String): Long?

    /**
     * Calls API to pack data
     * @param hash Hash to pack
     */
    suspend fun packData(hash: String): ByteArray

}
