package com.sword.signature.tezos.node.exception

import java.lang.RuntimeException

sealed class NodeException(message: String) : RuntimeException(message) {

    class NodeAccessException() : NodeException("The node is unreachable or produced an error.")

    class NotFound() : NodeException("The resource doesn't exist.")
}
