package com.sword.signature.tezos.service

import org.ej4tezos.model.TezosIdentity
import org.ej4tezos.papi.model.Ed25519KeyPair

/**
 * Regroups all operations with no interaction to the blockchain
 */
interface TezosUtilService {

    /**
     * Verify if the key pair is valid
     * @param publicKeyBase58 Signer public key in base58 string format.
     * @param secretKeyBase58 Signer secret key in base58 string format.
     */
    fun checkKeyPairValidity(publicKeyBase58: String, secretKeyBase58: String): Boolean

    /**
     * Retrieve the identity of the signer from its asymmetric keys.
     * @param publicKeyBase58 Signer public key in base58 string format.
     * @param secretKeyBase58 Signer secret key in base58 string format.
     * @return Crypto identity of the signer.
     */
    fun retrieveIdentity(publicKeyBase58: String, secretKeyBase58: String): TezosIdentity

    /**
     * Retrieve the identity of the signer from its key pair.
     * @param keyPair The key pair
     * @return Crypto identity of the signer.
     */
    fun retrieveIdentity(keyPair: Ed25519KeyPair): TezosIdentity

    /**
     * Unpack an address to a classic tz1 version.
     * @param packedAddress Address to unpack
     */
    fun unpackSignerAddress(packedAddress: String): String


    /**
     * Generates the script expr used to access the storage of a smart contract
     * @param packedData packed data to compute the script expr from
     */
    fun computeScriptExpr(packedData: ByteArray): String

}
