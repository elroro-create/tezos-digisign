package com.sword.signature.tezos.service.impl

import com.sword.signature.tezos.contract.HashTimestamping
import com.sword.signature.tezos.contract.RevealService
import com.sword.signature.tezos.contract.TransferService
import com.sword.signature.tezos.service.TezosUtilService
import com.sword.signature.tezos.service.TezosWriterService
import org.ej4tezos.api.TezosCoreService
import org.ej4tezos.api.TezosFeeService
import org.ej4tezos.api.TezosKeyService
import org.ej4tezos.api.exception.TezosException
import org.ej4tezos.crypto.impl.TezosCryptoProviderImpl
import org.ej4tezos.crypto.impl.TezosKeyServiceImpl
import org.ej4tezos.impl.SimpleTezosResourceLoader
import org.ej4tezos.impl.TezosConnectivityImpl
import org.ej4tezos.impl.TezosCoreServiceImpl
import org.ej4tezos.impl.TezosFeeServiceImpl
import org.ej4tezos.model.TezosContractAddress
import org.ej4tezos.model.TezosIdentity
import org.ej4tezos.papi.TezosConnectivity
import org.ej4tezos.papi.TezosCryptoProvider
import org.ej4tezos.papi.TezosResourceLoader
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.security.SecureRandom

@Service
class TezosWriterServiceImpl(
    @Value("\${tezos.contract.address}") private val contractAddress: String,
    @Value("\${tezos.node.url}") private val nodeUrl: String,
    private val tezosUtilService: TezosUtilService
) : TezosWriterService {
    private val tezosCryptoProvider: TezosCryptoProvider
    private val tezosConnectivity: TezosConnectivity
    private val tezosKeyService: TezosKeyService
    private val tezosCoreService: TezosCoreService
    private val tezosFeeService: TezosFeeService
    private val tezosResourceLoader: TezosResourceLoader

    private val hashTimestamping: HashTimestamping
    private val transferService: TransferService
    private val revealService: RevealService

    init {

        tezosResourceLoader = SimpleTezosResourceLoader()

        tezosCryptoProvider = TezosCryptoProviderImpl().apply {
            setSecureRandom(SecureRandom())
            init()
        }

        tezosConnectivity = TezosConnectivityImpl().apply {
            setNodeUrl(this@TezosWriterServiceImpl.nodeUrl)
            init()
        }

        tezosKeyService = TezosKeyServiceImpl().apply {
            setTezosCryptoProvider(this@TezosWriterServiceImpl.tezosCryptoProvider)
            init()
        }

        tezosCoreService = TezosCoreServiceImpl().apply {
            setTezosConnectivity(this@TezosWriterServiceImpl.tezosConnectivity)
            init()
        }

        tezosFeeService = TezosFeeServiceImpl().apply {
            init()
        }

        hashTimestamping = HashTimestamping().apply {
            setTezosContractAddress(TezosContractAddress.toTezosContractAddress(this@TezosWriterServiceImpl.contractAddress))


            setTezosResourceLoader(this@TezosWriterServiceImpl.tezosResourceLoader)
            setTezosKeyService(this@TezosWriterServiceImpl.tezosKeyService)
            setTezosFeeService(this@TezosWriterServiceImpl.tezosFeeService)
            setTezosCoreService(this@TezosWriterServiceImpl.tezosCoreService)
            setTezosConnectivity(this@TezosWriterServiceImpl.tezosConnectivity)
            setTezosCryptoProvider(this@TezosWriterServiceImpl.tezosCryptoProvider)
        }

        transferService = TransferService().apply {
            setTezosContractAddress(TezosContractAddress.toTezosContractAddress(this@TezosWriterServiceImpl.contractAddress))


            setTezosResourceLoader(this@TezosWriterServiceImpl.tezosResourceLoader)
            setTezosKeyService(this@TezosWriterServiceImpl.tezosKeyService)
            setTezosFeeService(this@TezosWriterServiceImpl.tezosFeeService)
            setTezosCoreService(this@TezosWriterServiceImpl.tezosCoreService)
            setTezosConnectivity(this@TezosWriterServiceImpl.tezosConnectivity)
            setTezosCryptoProvider(this@TezosWriterServiceImpl.tezosCryptoProvider)
        }

        revealService = RevealService().apply {
            setTezosContractAddress(TezosContractAddress.toTezosContractAddress(this@TezosWriterServiceImpl.contractAddress))


            setTezosResourceLoader(this@TezosWriterServiceImpl.tezosResourceLoader)
            setTezosKeyService(this@TezosWriterServiceImpl.tezosKeyService)
            setTezosFeeService(this@TezosWriterServiceImpl.tezosFeeService)
            setTezosCoreService(this@TezosWriterServiceImpl.tezosCoreService)
            setTezosConnectivity(this@TezosWriterServiceImpl.tezosConnectivity)
            setTezosCryptoProvider(this@TezosWriterServiceImpl.tezosCryptoProvider)
        }
    }

    @Throws(TezosException::class)
    override fun anchorHash(rootHash: String, signerIdentity: TezosIdentity): String {
        LOGGER.debug("anchorHash (rootHash = {}, signer = {})", rootHash, signerIdentity)
        hashTimestamping.apply {
            setAdminPrivateKey(signerIdentity.privateKey)
            init()
        }
        return hashTimestamping.timestampHash(rootHash)
    }

    @Throws(TezosException::class)
    override fun generateNewIdentity(): TezosIdentity {
        LOGGER.debug("generateNewIdentity")

        val keyPair =
            tezosCryptoProvider.generateEd25519KeyPair()

        val newIdentity = tezosUtilService.retrieveIdentity(keyPair)

        LOGGER.debug("generated new identity: {}", newIdentity)
        return newIdentity
    }

    @Throws(TezosException::class)
    override fun transferTezos(from: TezosIdentity, to: String, amount: Long): String {
        LOGGER.debug("transferTezos (from = {}, to = {}, amount = {})", from.publicAddress.value, to, amount)

        val transactionHash = transferService.transfer(from, to, amount)

        LOGGER.debug("transferred tezos (transactionId = {})", transactionHash)
        return transactionHash
    }

    @Throws(TezosException::class)
    override fun revealIdentity(identity: TezosIdentity): String {
        LOGGER.debug("revealIdentity (identity = {})", identity)

        val transactionHash = revealService.reveal(identity)

        LOGGER.debug("revealed an account (transactionId = {})", transactionHash)
        return transactionHash
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TezosWriterServiceImpl::class.java)
    }
}
