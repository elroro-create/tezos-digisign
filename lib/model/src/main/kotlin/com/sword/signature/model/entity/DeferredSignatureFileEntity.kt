package com.sword.signature.model.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "deferredSignatureFiles")
data class DeferredSignatureFileEntity(

    @Id
    val id: String? = null,

    val userId: String,

    val hash: String,

    val metadata: Metadata

)
