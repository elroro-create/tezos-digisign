package com.sword.signature.model.entity

import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.NotificationStatusType
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.OffsetDateTime

@Document(collection = "jobs")
sealed class TezosOperationJobEntity {

    abstract val id: String?

    /**
     * Date of the request
     */
    abstract val createdDate: OffsetDateTime

    /**
     * Date of the last injection in the block chain
     */
    abstract val injectedDate: OffsetDateTime?

    /**
     * Date of the validation in the block chain
     */
    abstract val validatedDate: OffsetDateTime?

    /**
     * Number of times we tried injecting the transaction in the blockchain
     */
    abstract val numberOfTry: Int

    /**
     * Hash of inject transaction
     */
    abstract val transactionHash: String?

    /**
     * Transaction timestamp.
     */
    abstract val timestamp: OffsetDateTime?

    abstract val blockHash: String?

    /**
     * Block depth at validation date.
     */
    abstract val blockDepth: Long?

    /**
     * Block depth of the head of the chain at anchoring and updated
     * to lastly checked depth in validation.
     */
    abstract val lastlyCheckedLevel: Long?

    /**
     * Date when the state was last updated
     */
    abstract var stateDate: OffsetDateTime

    abstract var state: JobStateType

    /**
     * Address of the transaction signer.
     */
    abstract val signerAddress: String?

    /**
     * User asking for the signature
     */
    abstract val userId: String

}

/**
 * Signature Job
 */
@TypeAlias("JobEntity")
data class JobEntity(
    @Id
    override val id: String? = null,
    @CreatedDate
    override val createdDate: OffsetDateTime = OffsetDateTime.now(),
    override val injectedDate: OffsetDateTime? = null,
    override val validatedDate: OffsetDateTime? = null,
    override val numberOfTry: Int = 0,
    override val transactionHash: String? = null,
    override val timestamp: OffsetDateTime? = null,
    override val blockHash: String? = null,
    override val blockDepth: Long? = null,
    override val lastlyCheckedLevel: Long? = null,
    override var stateDate: OffsetDateTime,
    override var state: JobStateType,
    override val signerAddress: String? = null,
    @Indexed
    override val userId: String,

    /**
     * Algorithm used to build the Merkel tree
     */
    val algorithm: String,

    /**
     * Whether or not an email should be sent to the requester when the job is completed
     */
    val emailNotification: String?,

    /**
     * The Origin request header indicates where a fetch originates from. Used to create a link to the UI
     */
    val origin: String?,

    /**
     * Name of the input Flow
     */
    val flowName: String,

    /**
     * url to call when job is anchored
     */
    val callBackUrl: String?,

    val callBackStatus: NotificationStatusType = NotificationStatusType.NONE, // default value for old job

    /**
     * Address of the smart contract in the blockchain.
     */
    val contractAddress: String? = null,

    /**
     * incoming channel of the job
     */
    val channelName: String? = null,

    /**
     * number of documents withing this jobs
     */
    val docsNumber: Int = 1, // TODO: remove the default value DIRTY !!

    /**
     * Metadata about the job filled by the signer.
     */
    val customFields: Map<String, String>? = null
) : TezosOperationJobEntity()

@TypeAlias("TransferJobEntity")
data class TransferJobEntity(
    @Id
    override val id: String? = null,
    @CreatedDate
    override val createdDate: OffsetDateTime = OffsetDateTime.now(),
    override val injectedDate: OffsetDateTime? = null,
    override val validatedDate: OffsetDateTime? = null,
    override val numberOfTry: Int = 0,
    override val transactionHash: String? = null,
    override val timestamp: OffsetDateTime? = null,
    override val blockHash: String? = null,
    override val blockDepth: Long? = null,
    override val lastlyCheckedLevel: Long? = null,
    override var stateDate: OffsetDateTime,
    override var state: JobStateType,
    override val signerAddress: String? = null,
    @Indexed
    override val userId: String,

    /**
     * Amount of tez to transfer, in micro tez
     */
    val amount: Long? = null,

    /**
     * The public address of the account to credit
     */
    val accountToCredit: String? = null,

    /**
     * The id of the Digisign account that needs a new tezos account
     */
    val accountToUpdate: String

) : TezosOperationJobEntity()

@TypeAlias("RevealJobEntity")
data class RevealJobEntity(
    @Id
    override val id: String? = null,
    @CreatedDate
    override val createdDate: OffsetDateTime = OffsetDateTime.now(),
    override val injectedDate: OffsetDateTime? = null,
    override val validatedDate: OffsetDateTime? = null,
    override val numberOfTry: Int = 0,
    override val transactionHash: String? = null,
    override val timestamp: OffsetDateTime? = null,
    override val blockHash: String? = null,
    override val blockDepth: Long? = null,
    override val lastlyCheckedLevel: Long? = null,
    override var stateDate: OffsetDateTime,
    override var state: JobStateType,
    override val signerAddress: String? = null,
    @Indexed
    override val userId: String,

    /**
     * The id of the Digisign account that needs a new tezos account
     */
    val accountToUpdate: String

) : TezosOperationJobEntity()
