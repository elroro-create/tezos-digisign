package com.sword.signature.model.configuration

import com.sword.signature.model.inheritanceaware.InheritanceAwareReactiveMongoRepositoryFactoryBean
import com.sword.signature.model.inheritanceaware.InheritanceAwareSimpleMongoRepository
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@Configuration
@EnableReactiveMongoRepositories(basePackages = ["com.sword.signature.model.repository"],repositoryBaseClass = InheritanceAwareSimpleMongoRepository::class,
repositoryFactoryBeanClass = InheritanceAwareReactiveMongoRepositoryFactoryBean::class)
class MongoConfiguration
