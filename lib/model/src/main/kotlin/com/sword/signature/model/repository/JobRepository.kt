package com.sword.signature.model.repository

import com.sword.signature.model.entity.JobEntity
import com.sword.signature.model.entity.RevealJobEntity
import com.sword.signature.model.entity.TezosOperationJobEntity
import com.sword.signature.model.entity.TransferJobEntity
import com.sword.signature.model.GenericJobRepository
import org.springframework.stereotype.Repository

@Repository
interface TezosOperationJobRepository : GenericJobRepository<TezosOperationJobEntity>

@Repository
interface JobRepository : GenericJobRepository<JobEntity>

@Repository
interface TransferJobRepository : GenericJobRepository<TransferJobEntity>

@Repository
interface RevealJobRepository : GenericJobRepository<RevealJobEntity>
