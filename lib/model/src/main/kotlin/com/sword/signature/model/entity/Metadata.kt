package com.sword.signature.model.entity

data class Metadata(
    val fileName: String,
    val fileSize: String? = null,
    val customFields: Map<String, String>? = null
)
