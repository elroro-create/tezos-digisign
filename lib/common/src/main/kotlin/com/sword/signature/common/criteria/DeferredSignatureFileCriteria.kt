package com.sword.signature.common.criteria

data class DeferredSignatureFileCriteria(
    val id: String? = null,
    val accountId: String? = null,
    val hash: String? = null,
    val name: String? = null,
)
