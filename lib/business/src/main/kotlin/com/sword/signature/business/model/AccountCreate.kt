package com.sword.signature.business.model

data class AccountCreate(
    val login: String,
    val email: String,
    val password: String,
    val fullName: String?,
    val company: String?,
    val country: String?,
    val tezosAccount: TezosAccount?,
    val isAdmin: Boolean = false,
    val disabled: Boolean = false,
    val signatureLimit: Int? = null,
    val isTezosAccountInConfig: Boolean? = null
)
