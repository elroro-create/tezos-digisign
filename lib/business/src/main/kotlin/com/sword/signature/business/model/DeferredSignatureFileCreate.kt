package com.sword.signature.business.model

data class DeferredSignatureFileCreate(
    val userId: String,
    val hash: String,
    val metadata: FileMetadata
)
