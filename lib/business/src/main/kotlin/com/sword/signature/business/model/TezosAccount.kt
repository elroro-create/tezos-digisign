package com.sword.signature.business.model

data class TezosAccount(
    val publicKey: String,
    val privateKey: String
)
