package com.sword.signature.business.model

import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.NotificationStatusType
import java.time.OffsetDateTime

sealed class TezosOperationJob {

    abstract val id: String

    /**
     * Date of the request
     */
    abstract val createdDate: OffsetDateTime

    /**
     * Date of the last injection in the block chain
     */
    abstract val injectedDate: OffsetDateTime?

    /**
     * Date of the validation in the block chain
     */
    abstract val validatedDate: OffsetDateTime?

    /**
     * Number of times we tried injecting the transaction in the blockchain
     */
    abstract val numberOfTry: Int

    /**
     * Hash of inject transaction
     */
    abstract val transactionHash: String?

    /**
     * Transaction timestamp.
     */
    abstract val timestamp: OffsetDateTime?

    abstract val blockHash: String?

    /**
     * Block depth at validation date.
     */
    abstract val blockDepth: Long?

    /**
     * Date when the state was last updated
     */
    abstract val stateDate: OffsetDateTime

    abstract val state: JobStateType

    /**
     * Address of the transaction signer.
     */
    abstract val signerAddress: String?

    /**
     * User asking for the signature
     */
    abstract val userId: String
}

data class Job(
    override val id: String,
    override val createdDate: OffsetDateTime = OffsetDateTime.now(),
    override val injectedDate: OffsetDateTime? = null,
    override val validatedDate: OffsetDateTime? = null,
    override val numberOfTry: Int = 0,
    override val transactionHash: String? = null,
    override val timestamp: OffsetDateTime? = null,
    override val blockHash: String? = null,
    override val blockDepth: Long? = null,
    override val stateDate: OffsetDateTime,
    override val state: JobStateType,
    override val signerAddress: String? = null,
    override val userId: String,

    /**
     * Algorithm used to build the Merkel tree
     */
    val algorithm: String,

    /**
     * Whether or not an email should be sent to the requester when the job is completed
     */
    val emailNotification: String? = null,

    /**
     * The Origin request header indicates where a fetch originates from. Used to create a link to the UI
     */
    val origin: String? = null,

    /**
     * Name of the input Flow
     */
    val flowName: String,

    /**
     * url to call when job is anchored
     */
    val callBackUrl: String? = null,

    val callBackStatus: NotificationStatusType,

    /**
     * Hash of root element
     */
    val rootHash: String? = null,

    val files: List<TreeElement.LeafTreeElement>? = null,

    /**
     * Address of the smart contract in the blockchain.
     */
    val contractAddress: String? = null,

    /**
     * incoming chanel of the job
     */
    val channelName: String? = null,

    /**
     * number of documents withing this jobs
     */
    val docsNumber: Int,

    /**
     * Metadata about the job filled by the signer.
     */
    val customFields: Map<String, String>? = null
) : TezosOperationJob()


data class TransferJob(
    override val id: String,
    override val createdDate: OffsetDateTime = OffsetDateTime.now(),
    override val injectedDate: OffsetDateTime? = null,
    override val validatedDate: OffsetDateTime? = null,
    override val numberOfTry: Int = 0,
    override val transactionHash: String? = null,
    override val timestamp: OffsetDateTime? = null,
    override val blockHash: String? = null,
    override val blockDepth: Long? = null,
    override val stateDate: OffsetDateTime,
    override val state: JobStateType,
    override val signerAddress: String? = null,
    override val userId: String,

    /**
     * Amount of tez to transfer, in micro tez
     */
    val amount: Long? = null,

    /**
     * The id of the Digisign account that needs a new tezos account
     */
    val accountToUpdate: String

) : TezosOperationJob()

data class RevealJob(
    override val id: String,
    override val createdDate: OffsetDateTime = OffsetDateTime.now(),
    override val injectedDate: OffsetDateTime? = null,
    override val validatedDate: OffsetDateTime? = null,
    override val numberOfTry: Int = 0,
    override val transactionHash: String? = null,
    override val timestamp: OffsetDateTime? = null,
    override val blockHash: String? = null,
    override val blockDepth: Long? = null,
    override val stateDate: OffsetDateTime,
    override val state: JobStateType,
    override val signerAddress: String? = null,
    override val userId: String,

    /**
     * The id of the Digisign account that needs a new tezos account
     */
    val accountToUpdate: String

) : TezosOperationJob()
