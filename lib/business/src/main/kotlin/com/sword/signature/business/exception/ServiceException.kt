package com.sword.signature.business.exception

import com.sword.signature.business.model.Account

/**
 * Server errors.
 * Translated to HTTP 5xx errors.
 */
open class ServiceException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(cause: Throwable) : super(cause)
}

/**
 * User input errors.
 * Translated to HTTP 4xx errors.
 */
open class UserServiceException : ServiceException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(cause: Throwable) : super(cause)
}

class EntityNotFoundException(name: String, id: String) : UserServiceException("The $name with id='$id' was not found.")
class AccountNotFoundException(loginOrEmail: String) :
    UserServiceException("The account with login or email '$loginOrEmail' was not found.")

class AlgorithmNotFoundException(algorithmName: String) :
    UserServiceException("The algorithm with name '$algorithmName' cannot be found.")

class PasswordTooWeakException() :
    UserServiceException("Password is too weak : must be 8 characters long, have 1 uppercase, 1 lowercase, 1 number and 1 special character")

class DuplicateException(message: String) : UserServiceException(message)
class MissingRightException(account: Account) :
    UserServiceException("The user ${account.login} doesn't have enough right to perform this operation.")

class InvalidSignatureLimitValueException() :
    UserServiceException("The signature limit must be a positive integer")

class TooManySignaturesTodayException() :
    UserServiceException("The number of daily signatures was exceeded for this Account")

class HoneyPotFieldNotEmptyException() :
    UserServiceException("A honeypot field was filled, request could be from a bot")

class EmailAddressIsDisposableException() :
    UserServiceException("Impossible to create an account with a disposable email address")

class TezosAccountAlreadyDefinedException() :
    UserServiceException("Tezos account on this account is already defined.")

class IncorrectTezosAccountException() :
    UserServiceException("There was a problem when verifying the key pair.")

class InvalidTezosKeyPairException() :
    UserServiceException("Key pair is not valid.")

class InvalidTezosAccountException() :
    UserServiceException("The tezos account doesn't exist or have no Tz.")

class FeatureNotEnabledException() :
    UserServiceException("This feature is not enabled in the configuration.")

class NoPasswordDefinedException() :
    UserServiceException("You need to set a password for this account.")
