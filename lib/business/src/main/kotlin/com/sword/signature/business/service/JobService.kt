package com.sword.signature.business.service

import com.sword.signature.business.model.*
import com.sword.signature.common.criteria.JobCriteria
import kotlinx.coroutines.flow.Flow
import org.springframework.data.domain.Pageable

interface JobService {

    /**
     * Retrieve all jobs matching the criteria.
     * @param requester The account requesting the jobs.
     * @param criteria Search criteria for job search.
     * @param pageable Pageable for memory-optimized job retrieval.
     * @return Jobs matching the criteria.
     */
    fun findAll(
        requester: Account,
        criteria: JobCriteria? = null,
        pageable: Pageable = Pageable.unpaged()
    ): Flow<TezosOperationJob>

    /**
     * Retrieve all signature jobs matching the criteria.
     * @param requester The account requesting the jobs.
     * @param criteria Search criteria for job search.
     * @param pageable Pageable for memory-optimized job retrieval.
     * @return Jobs matching the criteria.
     */
    fun findAllSignatureJob(
        requester: Account,
        criteria: JobCriteria? = null,
        pageable: Pageable = Pageable.unpaged()
    ): Flow<Job>

    /**
     * Retrieve all transfer jobs matching the criteria.
     * @param requester The account requesting the jobs.
     * @param criteria Search criteria for job search.
     * @param pageable Pageable for memory-optimized job retrieval.
     * @return Jobs matching the criteria.
     */
    fun findAllTransferJob(
        requester: Account,
        criteria: JobCriteria? = null,
        pageable: Pageable = Pageable.unpaged()
    ): Flow<TransferJob>

    /**
     * Retrieve all reveal jobs matching the criteria.
     * @param requester The account requesting the jobs.
     * @param criteria Search criteria for job search.
     * @param pageable Pageable for memory-optimized job retrieval.
     * @return Jobs matching the criteria.
     */
    fun findAllRevealJob(
        requester: Account,
        criteria: JobCriteria? = null,
        pageable: Pageable = Pageable.unpaged()
    ): Flow<RevealJob>

    /**
     * Count the number of signature jobs matching the criteria.
     * @param requester The account requesting the jobs.
     * @param criteria Search criteria for job search.
     * @return Number of jobs matching the criteria.
     */
    fun countAllSignatureJob(requester: Account, criteria: JobCriteria? = null): Long

    /**
     * Count the number of transfer jobs matching the criteria.
     * @param requester The account requesting the jobs.
     * @param criteria Search criteria for job search.
     * @return Number of jobs matching the criteria.
     */
    fun countAllTransferJob(requester: Account, criteria: JobCriteria? = null): Long

    /**
     * Count the number of reveal jobs matching the criteria.
     * @param requester The account requesting the jobs.
     * @param criteria Search criteria for job search.
     * @return Number of jobs matching the criteria.
     */
    fun countAllRevealJob(requester: Account, criteria: JobCriteria? = null): Long

    /**
     * Retrieve a  job by its id.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to retrieve.
     * @return The job if it exists, null otherwise.
     */
    suspend fun findByIdJob(requester: Account, jobId: String): TezosOperationJob?

    /**
     * Retrieve a signature job by its id.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to retrieve.
     * @param withLeaves Include or not the job merkle tree leaves in the returned object.
     * @return The job if it exists, null otherwise.
     */
    suspend fun findByIdSignatureJob(requester: Account, jobId: String, withLeaves: Boolean = false): Job?

    /**
     * Retrieve a transfer job by its id.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to retrieve.
     * @return The job if it exists, null otherwise.
     */
    suspend fun findByIdTransferJob(requester: Account, jobId: String): TransferJob?

    /**
     * Retrieve a reveal job by its id.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to retrieve.
     * @return The job if it exists, null otherwise.
     */
    suspend fun findByIdRevealJob(requester: Account, jobId: String): RevealJob?

    /**
     * Retrieve the merkel tree of a Job
     * @param requester The account requesting the job.
     * @param jobId Id of the job to retrieve.
     * @return The MerkelTree if it exists, null otherwise.
     */
    suspend fun getMerkelTree(requester: Account, jobId: String): MerkelTree?

    /**
     * Update a job with provided fields.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to update.
     * @param patch Fields to update.
     * @return The updated job.
     */
    suspend fun patchJob(requester: Account, jobId: String, patch: JobPatch): TezosOperationJob

    /**
     * Update a signature job with provided fields.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to update.
     * @param patch Fields to update.
     * @return The updated job.
     */
    suspend fun patchSignatureJob(requester: Account, jobId: String, patch: JobPatch): Job

    /**
     * Update a transfer job with provided fields.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to update.
     * @param patch Fields to update.
     * @return The updated job.
     */
    suspend fun patchTransferJob(requester: Account, jobId: String, patch: JobPatch): TransferJob

    /**
     * Update a reveal job with provided fields.
     * @param requester The account requesting the job.
     * @param jobId Id of the job to update.
     * @param patch Fields to update.
     * @return The updated job.
     */
    suspend fun patchRevealJob(requester: Account, jobId: String, patch: JobPatch): RevealJob

}
