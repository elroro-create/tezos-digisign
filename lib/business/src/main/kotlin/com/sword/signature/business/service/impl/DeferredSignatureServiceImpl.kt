package com.sword.signature.business.service.impl

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.exception.MissingRightException
import com.sword.signature.business.exception.ServiceException
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.DeferredSignatureFile
import com.sword.signature.business.model.DeferredSignatureFileCreate
import com.sword.signature.business.model.mapper.toBusiness
import com.sword.signature.business.model.mapper.toModel
import com.sword.signature.business.service.DeferredSignatureService
import com.sword.signature.common.criteria.DeferredSignatureFileCriteria
import com.sword.signature.model.entity.DeferredSignatureFileEntity
import com.sword.signature.model.mapper.toPredicate
import com.sword.signature.model.repository.DeferredSignatureFileRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitLast
import kotlinx.coroutines.reactive.awaitSingle
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class DeferredSignatureServiceImpl(
    private val deferredSignatureFileRepository: DeferredSignatureFileRepository
) : DeferredSignatureService {

    override suspend fun getFiles(
        requester: Account,
        criteria: DeferredSignatureFileCriteria?,
        pageable: Pageable
    ): Flow<DeferredSignatureFile> {

        if (!requester.isAdmin && requester.id != criteria?.accountId) {
            throw MissingRightException(requester)
        }

        return if (criteria != null) {
            deferredSignatureFileRepository.findAll(criteria.toPredicate(), pageable.sort)
        } else {
            deferredSignatureFileRepository.findAll(pageable.sort)
        }
            .asFlow()
            .paginate(pageable)
            .map { it.toBusiness() }

    }

    override suspend fun countFiles(
        requester: Account,
        criteria: DeferredSignatureFileCriteria?
    ): Long {
        // Check right to perform operation.
        if (!requester.isAdmin && requester.id != criteria?.accountId) {
            throw MissingRightException(requester)
        }

        return if (criteria != null) {
            deferredSignatureFileRepository.count(criteria.toPredicate())
        } else {
            deferredSignatureFileRepository.count()
        }.awaitLast()
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun deleteSignatureFile(requester: Account, id: String) {
        LOGGER.debug("Deleting deferred signature file with id '$id'")
        val file = deferredSignatureFileRepository.findById(id).awaitFirstOrNull()
            ?: throw EntityNotFoundException("deferredSignatureFile", id)
        // Check deletion right.
        if (!requester.isAdmin && requester.id != file.userId) {
            throw IllegalAccessException("Non-admin user ${requester.login} does not have permission to delete others' deferred signature files.")
        }
        deferredSignatureFileRepository.delete(file).awaitFirstOrNull()
        LOGGER.debug("Deferred signature file with id '$id' deleted.")
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun deferredSignature(
        requester: Account,
        signatureFileDetails: DeferredSignatureFileCreate
    ): DeferredSignatureFile {

        return deferredSignatureFileRepository.insert(
            DeferredSignatureFileEntity(
                userId = signatureFileDetails.userId,
                hash = signatureFileDetails.hash,
                metadata = signatureFileDetails.metadata.toModel()
            )
        ).awaitSingle().toBusiness()

    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(DeferredSignatureServiceImpl::class.java)
    }
}
