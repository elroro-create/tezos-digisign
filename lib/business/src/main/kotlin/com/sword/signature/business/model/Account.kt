package com.sword.signature.business.model

import com.sword.signature.common.enums.TezosAccountStateType
import java.io.Serializable

data class Account(
    val id: String,
    val login: String,
    val email: String,
    val password: String,
    val fullName: String?,
    val company: String?,
    val country: String?,
    val publicKey: String?,
    val privateKey: String?,
    val hash: String?,
    val isAdmin: Boolean,
    val disabled: Boolean,
    val firstLogin: Boolean,
    val signatureLimit: Int?,
    val tezosAccountState: TezosAccountStateType = TezosAccountStateType.IN_CONFIGURATION
) : Serializable
