package com.sword.signature.business.service


import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.Job
import com.sword.signature.business.model.JobPatch
import com.sword.signature.common.criteria.JobCriteria
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.NotificationStatusType
import com.sword.signature.model.migration.MigrationHandler
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import java.nio.file.Path
import java.time.LocalDate
import java.time.OffsetDateTime
import java.util.stream.Stream

class JobServiceContextTest @Autowired constructor(
    override val migrationHandler: MigrationHandler,
    override val mongoTemplate: ReactiveMongoTemplate,

    private val jobService: JobService
) : AbstractServiceContextTest() {

    private val simpleAccount = Account(
        id = "ljghdslkgjsdglhjdslghjdsklgjdgskldjglgdsjgdlskgjdslknjcvhuire", login = "simple", password = "password",
        fullName = "simple user", email = "simplie@toto.net", company = null, country = null, publicKey = null,
        privateKey = null, hash = null, isAdmin = false, disabled = false, firstLogin = false, signatureLimit = null
    )

    private val adminAccount = simpleAccount.copy(
        id = "5e74a073a386f170f3850b4b",
        isAdmin = true,
    )

    private val secondAdmin = adminAccount.copy(
        id = "ljghdslkgjsdglhgskldjglgdsjgdlskgjdslknjcvhuire",
    )

    // signature jobs
    private val multipleFileJobId = "5e8c36c49df469062bc658c1"
    private val singleFileJobId = "5e8c36c49df469062bc658c8"

    private val simpUserTransferJobId1 = "5e8c36c49df469062bc658d8"
    private val simpUserTransferJobId2 = "5e8c36c49df469062bc658d9"
    private val adminTransferJobId1 = "5e8c36c49df469062bc658e0"
    private val adminTransferJobId2 = "5e8c36c49df469062bc658e1"

    private val simpUserRevealJobId1 = "5e8c36c49df469062bc658e2"
    private val simpUserRevealJobId2 = "5e8c36c49df469062bc658e3"
    private val adminRevealJobId1 = "5e8c36c49df469062bc658e4"
    private val adminRevealJobId2 = "5e8c36c49df469062bc658e5"

    private val singleUserJob1Id = "5e8c36c49df469062bc658c9"
    private val singleUserJob2Id = "5e8c36c49df469062bc658d0"
    private val singleUserJob3Id = "5e8c36c49df469062bc658d6"
    private val singleUserJob4Id = "5e8c36c49df469062bc658d7"


    private val multipleFileJob = Job(
        id = multipleFileJobId,
        createdDate = OffsetDateTime.parse("2020-04-07T08:16:04.028Z"),
        numberOfTry = 0,
        algorithm = "SHA-256",
        userId = "5e74a073a386f170f3850b4b",
        flowName = "ARS_20180626_02236_130006",
        stateDate = OffsetDateTime.parse("2020-04-07T08:16:04.028Z"),
        state = JobStateType.INSERTED,
        callBackStatus = NotificationStatusType.PENDING,
        docsNumber = 3
    )

    private val singleFileJob = Job(
        id = singleFileJobId,
        createdDate = OffsetDateTime.parse("2020-04-08T08:16:04.114Z"),
        numberOfTry = 0,
        algorithm = "SHA-256",
        userId = "5e74a073a386f170f3850b4b",
        flowName = "ARS_20180626_02236_130006",
        stateDate = OffsetDateTime.parse("2020-04-07T08:16:04.113Z"),
        state = JobStateType.INSERTED,
        callBackStatus = NotificationStatusType.PENDING,
        channelName = "chaine editique",
        docsNumber = 1
    )

    @BeforeEach
    fun refreshDatabase() {
        resetDatabase()
        LOGGER.debug("Loading jobs from resources")
        importJsonDatasets(Path.of("src/test/resources/datasets/jobs.json"))
    }

    @Nested
    inner class FindAll {

        @Nested
        inner class `Simple user can't list` {

            @Test
            fun `other's jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAll(simpleAccount, JobCriteria(accountId = adminAccount.id)).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

            @Test
            fun `other's signature jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAllSignatureJob(simpleAccount, JobCriteria(accountId = adminAccount.id)).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

            @Test
            fun `other's transfer jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAllTransferJob(simpleAccount, JobCriteria(accountId = adminAccount.id)).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

            @Test
            fun `other's reveal jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAllRevealJob(simpleAccount, JobCriteria(accountId = adminAccount.id)).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

            @Test
            fun `all's jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAll(simpleAccount).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

            @Test
            fun `all's signature jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAllSignatureJob(simpleAccount).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

            @Test
            fun `all's transfer jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAllTransferJob(simpleAccount).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

            @Test
            fun `all's reveal jobs`() {

                Assertions.assertThatThrownBy {
                    runBlocking {
                        jobService.findAllRevealJob(simpleAccount).toList()
                    }
                }.isInstanceOf(IllegalAccessException::class.java)

            }

        }

        @Nested
        inner class `Admin can list his own jobs` {

            @Test
            fun `all jobs`() {
                runBlocking {
                    val actual =
                        jobService.findAll(adminAccount, JobCriteria(accountId = adminAccount.id))
                            .map { it.id }
                            .toList()

                    val expected = listOf(
                        multipleFileJobId,
                        singleFileJobId,
                        adminTransferJobId1,
                        adminTransferJobId2,
                        adminRevealJobId1,
                        adminRevealJobId2
                    )

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `signature jobs`() {
                runBlocking {
                    val actual =
                        jobService.findAllSignatureJob(adminAccount, JobCriteria(accountId = adminAccount.id))
                            .map { it.id }
                            .toList()

                    val expected = listOf(multipleFileJobId, singleFileJobId)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `transfer jobs`() {
                runBlocking {
                    val actual =
                        jobService.findAllTransferJob(adminAccount, JobCriteria(accountId = adminAccount.id))
                            .map { it.id }
                            .toList()

                    val expected = listOf(adminTransferJobId1, adminTransferJobId2)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `reveal jobs`() {
                runBlocking {
                    val actual =
                        jobService.findAllRevealJob(adminAccount, JobCriteria(accountId = adminAccount.id))
                            .map { it.id }
                            .toList()

                    val expected = listOf(adminRevealJobId1, adminRevealJobId2)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

        }

        @Nested
        inner class `Filter Signature Job` {

            @Test
            fun `filter by id`() {
                runBlocking {
                    val actual =
                        jobService.findAllSignatureJob(adminAccount, JobCriteria(id = "5e8c36c49df469062bc658d0"))
                            .map { it.id }
                            .toList()

                    val expected = listOf(singleUserJob2Id)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by flowName`() {
                runBlocking {
                    val actual =
                        jobService.findAllSignatureJob(adminAccount, JobCriteria(flowName = "rs_20180626_02236_13000"))
                            .map { it.id }
                            .toList()

                    val expected = listOf(multipleFileJobId, singleFileJobId)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by start date`() {
                runBlocking {
                    val actual =
                        jobService.findAllSignatureJob(adminAccount, JobCriteria(dateStart = LocalDate.of(2020, 4, 8)))
                            .map { it.id }
                            .toList()

                    val expected =
                        listOf(singleFileJobId, singleUserJob1Id, singleUserJob2Id, singleUserJob3Id, singleUserJob4Id)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by end date`() {
                runBlocking {
                    val actual =
                        jobService.findAllSignatureJob(adminAccount, JobCriteria(dateEnd = LocalDate.of(2020, 4, 8)))
                            .map { it.id }
                            .toList()

                    val expected = listOf(multipleFileJobId, singleFileJobId)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by channel name`() {
                runBlocking {
                    val actual =
                        jobService.findAllSignatureJob(adminAccount, JobCriteria(channelName = "ine edit"))
                            .map { it.id }
                            .toList()

                    val expected = listOf(singleFileJobId, singleUserJob1Id)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

        }

        @Nested
        inner class `Filter Transfer Job` {

            @Test
            fun `filter by id`() {
                runBlocking {
                    val actual =
                        jobService.findAllTransferJob(adminAccount, JobCriteria(id = "5e8c36c49df469062bc658e0"))
                            .map { it.id }
                            .toList()

                    val expected = listOf(adminTransferJobId1)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by start date`() {
                runBlocking {
                    val actual =
                        jobService.findAllTransferJob(adminAccount, JobCriteria(dateStart = LocalDate.of(2020, 4, 8)))
                            .map { it.id }
                            .toList()

                    val expected =
                        listOf(adminTransferJobId1, simpUserTransferJobId1, simpUserTransferJobId2)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by end date`() {
                runBlocking {
                    val actual =
                        jobService.findAllTransferJob(adminAccount, JobCriteria(dateEnd = LocalDate.of(2020, 4, 8)))
                            .map { it.id }
                            .toList()

                    val expected = listOf(adminTransferJobId2)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

        }

        @Nested
        inner class `Filter Reveal Job` {

            @Test
            fun `filter by id`() {
                runBlocking {
                    val actual =
                        jobService.findAllRevealJob(adminAccount, JobCriteria(id = "5e8c36c49df469062bc658e4"))
                            .map { it.id }
                            .toList()

                    val expected = listOf(adminRevealJobId1)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by start date`() {
                runBlocking {
                    val actual =
                        jobService.findAllRevealJob(adminAccount, JobCriteria(dateStart = LocalDate.of(2020, 4, 8)))
                            .map { it.id }
                            .toList()

                    val expected =
                        listOf(adminRevealJobId1, simpUserRevealJobId1, simpUserRevealJobId2)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

            @Test
            fun `filter by end date`() {
                runBlocking {
                    val actual =
                        jobService.findAllRevealJob(adminAccount, JobCriteria(dateEnd = LocalDate.of(2020, 4, 8)))
                            .map { it.id }
                            .toList()

                    val expected = listOf(adminRevealJobId2)

                    assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
                }
            }

        }

        @Test
        fun `second admin get first's jobs list`() {
            runBlocking {
                val actual =
                    jobService.findAllSignatureJob(secondAdmin, JobCriteria(accountId = adminAccount.id)).map { it.id }
                        .toList()

                val expected = listOf(multipleFileJobId, singleFileJobId)

                assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
            }
        }

        @Test
        fun `admin get all signature jobs list`() {
            runBlocking {
                val actual = jobService.findAllSignatureJob(adminAccount).map { it.id }.toList() // nono

                val expected = listOf(
                    multipleFileJobId, singleFileJobId, singleUserJob1Id, singleUserJob2Id,
                    singleUserJob3Id, singleUserJob4Id
                )

                assertThat(actual).containsExactlyInAnyOrderElementsOf(expected)
            }
        }

        @Test
        fun `sort by channel name`() {
            runBlocking {
                val pageable = PageRequest.of(0, 30, Sort.Direction.ASC, "channelName")

                val actual =
                    jobService.findAllSignatureJob(requester = adminAccount, pageable = pageable).map { it.id }
                        .toList()

                val expected = listOf(
                    multipleFileJobId, singleUserJob2Id, singleUserJob3Id, singleUserJob4Id, singleFileJobId,
                    singleUserJob1Id
                )

                assertThat(actual).containsExactlyElementsOf(expected)
            }
        }

        @Test
        fun `sort by flowName`() {
            runBlocking {
                val pageable = PageRequest.of(0, 30, Sort.Direction.DESC, "flowName")
                val actual =
                    jobService.findAllSignatureJob(requester = adminAccount, pageable = pageable).map { it.id }
                        .toList()

                val expected = listOf(
                    singleUserJob4Id,
                    singleUserJob3Id,
                    singleUserJob2Id,
                    singleUserJob1Id,
                    multipleFileJobId,
                    singleFileJobId
                )
                assertThat(actual).containsExactlyElementsOf(expected)
            }
        }

        @Test
        fun `first page of two`() {
            runBlocking {
                val pageable = PageRequest.of(0, 2, Sort.Direction.DESC, "flowName")
                val actual =
                    jobService.findAllSignatureJob(requester = adminAccount, pageable = pageable).map { it.id }
                        .toList()

                val expected = listOf(singleUserJob4Id, singleUserJob3Id)
                assertThat(actual).containsExactlyElementsOf(expected)
            }
        }

        @Test
        fun `second page of two`() {
            runBlocking {
                val pageable = PageRequest.of(1, 2, Sort.Direction.DESC, "flowName")
                val actual =
                    jobService.findAllSignatureJob(requester = adminAccount, pageable = pageable).map { it.id }
                        .toList()

                val expected = listOf(singleUserJob2Id, singleUserJob1Id)
                assertThat(actual).containsExactlyElementsOf(expected)
            }
        }

    }

    @Nested
    inner class FindById {

        @Test
        fun `simple user can't have a job not his own`() {
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.findByIdSignatureJob(simpleAccount, multipleFileJobId)
                }
            }.isInstanceOf(IllegalAccessException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.findByIdTransferJob(simpleAccount, adminTransferJobId1)
                }
            }.isInstanceOf(IllegalAccessException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.findByIdRevealJob(simpleAccount, adminRevealJobId1)
                }
            }.isInstanceOf(IllegalAccessException::class.java)
        }

        @Test
        fun `not existing job should return null for authorized person`() {
            runBlocking {
                val job = jobService.findByIdSignatureJob(secondAdmin, "falseId")
                assertThat(job).isNull()
                val job2 = jobService.findByIdTransferJob(secondAdmin, "falseId")
                assertThat(job2).isNull()
                val job3 = jobService.findByIdRevealJob(secondAdmin, "falseId")
                assertThat(job3).isNull()
            }
        }

        @Test
        fun `not existing job should return null for everybody`() {
            runBlocking {
                val job = jobService.findByIdSignatureJob(simpleAccount, "falseId")
                assertThat(job).isNull()
                val job2 = jobService.findByIdTransferJob(simpleAccount, "falseId")
                assertThat(job2).isNull()
                val job3 = jobService.findByIdRevealJob(simpleAccount, "falseId")
                assertThat(job3).isNull()
            }
        }

        @ParameterizedTest
        @MethodSource("getSingleJobProvider")
        fun getSingleJob(
            requesterAccount: Account,
            jobId: String,
            expected: Job,
            expectedFileNames: List<String>
        ) {
            runBlocking {
                val job = jobService.findByIdSignatureJob(requesterAccount, jobId, true)
                assertThat(job).isNotNull
                job as Job
                assertThat(job).`as`("mauvais job").usingRecursiveComparison().ignoringFields("rootHash", "files")
                    .isEqualTo(expected)
                assertThat(job.files?.map { it.metadata.fileName }).`as`("pas de file").isNotNull.`as`("mauvais files")
                    .containsExactlyInAnyOrderElementsOf(expectedFileNames)
            }
        }

        @ParameterizedTest
        @MethodSource("getSingleJobProvider")
        fun getSingleJobNoLeaf(
            requesterAccount: Account,
            jobId: String,
            expected: Job,
            expectedFileNames: List<String>
        ) {
            runBlocking {
                val job = jobService.findByIdSignatureJob(requesterAccount, jobId)
                assertThat(job).isNotNull
                job as Job
                assertThat(job).`as`("mauvais job").usingRecursiveComparison().ignoringFields("rootHash", "files")
                    .isEqualTo(expected)
                assertThat(job.files).`as`("pas de file").isNull()
            }
        }

        fun getSingleJobProvider(): Stream<Arguments> {
            return listOf(
                Arguments.of(
                    secondAdmin,
                    multipleFileJobId,
                    multipleFileJob,
                    listOf("ARS_02236_00004.pdf", "ARS_02236_00002.pdf", "ARS_02236_00001.pdf")
                ), Arguments.of(
                    secondAdmin,
                    singleFileJobId,
                    singleFileJob,
                    listOf("ARS_02236_00003.pdf")
                )
            ).stream()
        }
    }

    @Nested
    inner class Update {

        @Test
        fun `simple user can't update a job not his own`() {
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchSignatureJob(simpleAccount, multipleFileJobId, JobPatch())
                }
            }.isInstanceOf(IllegalAccessException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchTransferJob(simpleAccount, adminTransferJobId1, JobPatch())
                }
            }.isInstanceOf(IllegalAccessException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchRevealJob(simpleAccount, adminRevealJobId1, JobPatch())
                }
            }.isInstanceOf(IllegalAccessException::class.java)
        }

        @Test
        fun `not existing job should thrown exception`() {
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchSignatureJob(secondAdmin, "falseId", JobPatch())
                }
            }.isInstanceOf(EntityNotFoundException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchTransferJob(secondAdmin, "falseId", JobPatch())
                }
            }.isInstanceOf(EntityNotFoundException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchRevealJob(secondAdmin, "falseId", JobPatch())
                }
            }.isInstanceOf(EntityNotFoundException::class.java)
        }

        @Test
        fun `not existing job should thrown exception for everybody`() {
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchSignatureJob(simpleAccount, "falseId", JobPatch())
                }
            }.isInstanceOf(EntityNotFoundException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchTransferJob(simpleAccount, "falseId", JobPatch())
                }
            }.isInstanceOf(EntityNotFoundException::class.java)
            Assertions.assertThatThrownBy {
                runBlocking {
                    jobService.patchRevealJob(simpleAccount, "falseId", JobPatch())
                }
            }.isInstanceOf(EntityNotFoundException::class.java)
        }

        @ParameterizedTest
        @MethodSource("updateJobProvider")
        fun `working update`(
            comment: String,
            requester: Account,
            jobId: String,
            patch: JobPatch,
            expected: Job
        ) {
            runBlocking {
                val before = OffsetDateTime.now()
                val job = jobService.patchSignatureJob(requester, jobId, patch)

                assertThat(job).`as`(comment)
                    .usingRecursiveComparison().ignoringFields("injectedDate", "validatedDate", "stateDate")
                    .isEqualTo(expected)
                if (patch.state == JobStateType.INJECTED) {
                    assertThat(job.injectedDate).`as`("verification date injection").isAfter(before)
                }
                if (patch.state == JobStateType.VALIDATED) {
                    assertThat(job.validatedDate).`as`("verification date validation").isAfter(before)
                }
            }
        }

        fun updateJobProvider() = Stream.of(
            Arguments.of(
                "no change",
                secondAdmin,
                multipleFileJobId,
                JobPatch(),
                multipleFileJob
            ),
            Arguments.of(
                "numbreOfTry change",
                secondAdmin,
                multipleFileJobId,
                JobPatch(numberOfTry = 18),
                multipleFileJob.copy(numberOfTry = 18)
            ),
            Arguments.of(
                "blockId change",
                adminAccount,
                multipleFileJobId,
                JobPatch(blockHash = "superbeId"),
                multipleFileJob.copy(blockHash = "superbeId")
            ),
            Arguments.of(
                "blockDepth change",
                adminAccount,
                multipleFileJobId,
                JobPatch(blockDepth = 56),
                multipleFileJob.copy(blockDepth = 56)
            ),
            Arguments.of(
                "state change to INJECTED",
                adminAccount,
                multipleFileJobId,
                JobPatch(state = JobStateType.INJECTED),
                multipleFileJob.copy(state = JobStateType.INJECTED)
            ),
            Arguments.of(
                "state change to VALIDATED",
                adminAccount,
                multipleFileJobId,
                JobPatch(state = JobStateType.VALIDATED),
                multipleFileJob.copy(state = JobStateType.VALIDATED)
            ),
            Arguments.of(
                "callback status to COMPLETED",
                adminAccount,
                multipleFileJobId,
                JobPatch(callBackStatus = NotificationStatusType.COMPLETED),
                multipleFileJob.copy(callBackStatus = NotificationStatusType.COMPLETED)
            )

        )

    }

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(JobServiceContextTest::class.java)
    }
}
