package com.sword.signature.api.auth

data class ForgotPwdResponse(
    val maskedEmail: String
)
