package com.sword.signature.api.sign

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class DeferredSignatureFile(
    val id: String,
    val hash: String,
    val metadata: SignMetadata,
)
