package com.sword.signature.api.sign

data class DeferredSignatureResponse (
    val files: List<SignMetadata>
)
