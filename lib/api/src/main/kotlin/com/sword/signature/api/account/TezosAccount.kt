package com.sword.signature.api.account

data class TezosAccount(
    val publicKey: String,
    val privateKey: String
)
