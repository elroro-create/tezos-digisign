package com.sword.signature.daemon.job.validation.impl

import com.sword.signature.business.model.Account
import com.sword.signature.business.model.integration.CallBackJobMessagePayload
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.model.mail.AnchorNotificationMail
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.business.service.MailService
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.job.validation.TezosOperationValidationJob
import com.sword.signature.daemon.logger
import com.sword.signature.daemon.sendPayload
import com.sword.signature.tezos.node.NodeConnector
import com.sword.signature.tezos.service.TezosReaderService
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.MessageChannel
import org.springframework.stereotype.Component
import java.time.Duration

@Component
class ValidateSignatureJob(
    override val jobService: JobService,
    override val nodeConnector: NodeConnector,
    override val accountService: AccountService,
    override val tezosReaderService: TezosReaderService,
    override val anchoringMessageChannel: MessageChannel,
    override val validationRetryMessageChannel: MessageChannel,
    @Value("\${tezos.validation.minDepth}") override val minDepth: Long,
    @Value("\${daemon.validation.timeout}") override val validationTimeout: Duration,
    override val metrics: Metrics,

    private val callbackMessageChannel: MessageChannel,
    private val mailService: MailService
) : TezosOperationValidationJob {

    override suspend fun onValidated(
        requester: Account,
        payload: TezosOperationValidationJobPayload
    ) {
        val job = jobService.findByIdSignatureJob(requester, payload.jobId)
            ?: throw IllegalStateException("The transfer job with id = ${payload.jobId} should exist")

        // Send an email to notify of job validated
        if (job.emailNotification != null && job.origin != null) {
            mailService.sendEmail(
                AnchorNotificationMail(
                    recipient = requester.copy(email = job.emailNotification!!),
                    flowName = job.flowName,
                    docsNumber = job.docsNumber,
                    jobId = job.id,
                    injectedDate = job.injectedDate,
                    origin = job.origin!!
                )
            )
        }

        // Call the callback url
        if (job.callBackUrl != null) {
            callbackMessageChannel.sendPayload(
                CallBackJobMessagePayload(
                    jobId = job.id,
                    url = job.callBackUrl!!
                )
            )
        }
    }

    override suspend fun onValidationFailed(requester: Account, payload: TezosOperationValidationJobPayload) {
        anchoringMessageChannel.sendPayload(
            TezosOperationAnchorJobPayload(
                type = JobPayloadType.SIGNATURE,
                requesterId = requester.id,
                jobId = payload.jobId
            )
        )
    }

    companion object {
        val LOGGER = logger()
    }

}
