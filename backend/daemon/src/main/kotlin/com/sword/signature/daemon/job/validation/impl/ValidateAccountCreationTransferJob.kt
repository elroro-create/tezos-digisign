package com.sword.signature.daemon.job.validation.impl

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.TezosAccountStateType
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.job.validation.TezosOperationValidationJob
import com.sword.signature.daemon.logger
import com.sword.signature.daemon.sendPayload
import com.sword.signature.model.entity.RevealJobEntity
import com.sword.signature.model.repository.AccountRepository
import com.sword.signature.model.repository.RevealJobRepository
import com.sword.signature.tezos.node.NodeConnector
import com.sword.signature.tezos.service.TezosReaderService
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.OffsetDateTime

@Component
class ValidateAccountCreationTransferJob(
    override val jobService: JobService,
    override val nodeConnector: NodeConnector,
    override val accountService: AccountService,
    override val tezosReaderService: TezosReaderService,
    override val anchoringMessageChannel: MessageChannel,
    override val validationRetryMessageChannel: MessageChannel,
    @Value("\${tezos.validation.minDepth}") override val minDepth: Long,
    @Value("\${daemon.validation.timeout}") override val validationTimeout: Duration,
    override val metrics: Metrics,

    private val revealJobRepository: RevealJobRepository,
    private val accountRepository: AccountRepository,
) : TezosOperationValidationJob {

    override suspend fun onValidated(
        requester: Account,
        payload: TezosOperationValidationJobPayload
    ) {

        val transferJob = jobService.findByIdTransferJob(requester, payload.jobId)
            ?: throw IllegalStateException("TransferJob with id = ${payload.jobId} should exist")

        val accountToUpdate = accountRepository.findById(transferJob.accountToUpdate).awaitFirstOrNull()
            ?: throw EntityNotFoundException("account", transferJob.accountToUpdate)

        if (accountToUpdate.publicKey.isNullOrBlank() || accountToUpdate.privateKey.isNullOrBlank() || accountToUpdate.hash.isNullOrBlank()) {
            throw IllegalStateException("New account's tezos account should be set by now.")
        }

        // Sanity check, tez balance of credited account should not be 0.
        if (tezosReaderService.getTezosAccountBalance(accountToUpdate.hash!!) <= 0) {
            throw IllegalStateException("New account should have balance != 0 by now.")
        }

        // Update account to notify of account state
        val toPatch = accountToUpdate.copy(
            tezosAccountState = TezosAccountStateType.REVEALING
        )
        accountRepository.save(toPatch).awaitSingle()

        // The second thing to do when creating a tezos account is reveal their public key to the chain.
        // Creating the RevealJob.
        val revealJobEntity = revealJobRepository.insert(
            RevealJobEntity(
                state = JobStateType.INSERTED,
                stateDate = OffsetDateTime.now(),
                userId = requester.id,
                accountToUpdate = transferJob.accountToUpdate
            )
        ).awaitSingle()

        // Send a message to the anchoring channel to trigger the daemon anchoring job
        anchoringMessageChannel.send(
            MessageBuilder.withPayload(
                TezosOperationAnchorJobPayload(
                    type = JobPayloadType.REVEAL,
                    requesterId = requester.id,
                    jobId = revealJobEntity.id!!
                )
            ).build()
        )
    }

    override suspend fun onValidationFailed(requester: Account, payload: TezosOperationValidationJobPayload) {
        anchoringMessageChannel.sendPayload(
            TezosOperationAnchorJobPayload(
                type = JobPayloadType.ACCOUNT_CREATION_TRANSFER,
                requesterId = requester.id,
                jobId = payload.jobId
            )
        )
    }

    companion object {
        val LOGGER = logger()
    }

}
