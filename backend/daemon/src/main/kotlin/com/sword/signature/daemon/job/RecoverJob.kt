package com.sword.signature.daemon.job

import com.sword.signature.business.model.Job
import com.sword.signature.business.model.RevealJob
import com.sword.signature.business.model.TransferJob
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.service.JobService
import com.sword.signature.business.service.dummyAdminAccount
import com.sword.signature.common.criteria.JobCriteria
import com.sword.signature.common.enums.JobStateType
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component

@Component
class RecoverJob(
    private val jobService: JobService,
    private val anchoringRetryMessageChannel: MessageChannel
) {

    fun recoverInsertedJobs() {
        val insertedJobs =
            runBlocking {
                jobService.findAll(dummyAdminAccount, JobCriteria(jobState = JobStateType.INSERTED)).toList()
            }
        LOGGER.info("{} inserted jobs retrieved at startup.", insertedJobs.size)

        for (job in insertedJobs) {
            val jobType = when (job) {
                is Job -> JobPayloadType.SIGNATURE
                is TransferJob -> JobPayloadType.ACCOUNT_CREATION_TRANSFER
                is RevealJob -> JobPayloadType.REVEAL
            }
            anchoringRetryMessageChannel.send(
                MessageBuilder.withPayload(
                    TezosOperationAnchorJobPayload(
                        type = jobType,
                        requesterId = job.userId,
                        jobId = job.id
                    )
                ).build()
            )
        }
        LOGGER.info("{} inserted jobs recovered.", insertedJobs.size)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(RecoverJob::class.java)
    }
}
