package com.sword.signature.daemon.job.anchor

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.model.*
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.business.service.dummyAdminAccount
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.TezosAccountStateType
import com.sword.signature.daemon.configuration.TezosConfig
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.job.anchor.impl.AnchorRevealJob
import com.sword.signature.daemon.logger
import com.sword.signature.tezos.node.NodeConnector
import com.sword.signature.tezos.service.TezosReaderService
import com.sword.signature.tezos.service.TezosUtilService
import com.sword.signature.tezos.service.TezosWriterService
import org.ej4tezos.model.TezosIdentity
import org.springframework.messaging.MessageChannel

interface TezosOperationAnchorJob {

    val accountService: AccountService
    val tezosWriterService: TezosWriterService
    val tezosUtilService: TezosUtilService
    val tezosReaderService: TezosReaderService
    val jobService: JobService
    val nodeConnector: NodeConnector

    val anchoringRetryMessageChannel: MessageChannel
    val validationMessageChannel: MessageChannel
    val tezosConfig: TezosConfig
    val metrics: Metrics

    suspend fun anchor(payload: TezosOperationAnchorJobPayload) {
        LOGGER.debug("Start anchoring an operation of type '${payload.type}', requested by '${payload.requesterId}', with job '${payload.jobId}'.")
        startAnchor(payload)
    }

    suspend fun startAnchor(payload: TezosOperationAnchorJobPayload)

    suspend fun checkTezosIdentity(requester: Account): TezosIdentity {
        val requesterKeys = checkTezosKeys(requester) // Gets keys from account
        return tezosUtilService.retrieveIdentity(
            publicKeyBase58 = requesterKeys.first,
            secretKeyBase58 = requesterKeys.second
        )
    }

    suspend fun getHeadBlockLevel(): Long {
        return nodeConnector.getHeadBlockLevel()
    }

    suspend fun getAccount(accountId: String): Account {
        return accountService.getAccount(dummyAdminAccount, accountId) ?: throw EntityNotFoundException(
            "account",
            accountId
        )
    }

    /**
     * Rejects a job and logs the reason
     */
    suspend fun rejectJob(message: String, requester: Account, jobId: String) {
        AnchorRevealJob.LOGGER.info("Job was rejected because: $message.")

        val job = jobService.patchJob(
            requester = requester,
            jobId = jobId,
            patch = JobPatch(
                state = JobStateType.REJECTED
            )
        )

        when (job) {
            is Job -> metrics.getKOSignatureOperationCounter().increment()
            is RevealJob -> metrics.getKORevealOperationCounter().increment()
            is TransferJob -> metrics.getKOTransferOperationCounter().increment()
        }

    }

    private fun checkTezosKeys(account: Account): Pair<String, String> {
        LOGGER.debug("Checking keys for {}.", account.login)
        return if (account.tezosAccountState === TezosAccountStateType.IN_CONFIGURATION) {
            getTezosKeysFromConfig(account.login)
        } else {
            val publicKey: String =
                account.publicKey ?: throw IllegalStateException("Public key not provided for ${account.login}.")
            val secretKey: String =
                account.privateKey ?: throw IllegalStateException("Secret key not provided for ${account.login}.")
            Pair(publicKey, secretKey)
        }
    }

    private fun getTezosKeysFromConfig(accountLogin: String): Pair<String, String> {
        LOGGER.debug("Retrieving keys for {}., accountLogin")
        val accountKeys: Map<String, String> = tezosConfig.keys[accountLogin]
            ?: throw IllegalStateException("Keys for are not provided by configuration file.")
        val publicKey: String =
            accountKeys["publicKey"] ?: throw IllegalStateException("Public key not provided for .")
        val secretKey: String =
            accountKeys["secretKey"] ?: throw IllegalStateException("Secret key not provided for .")
        return Pair(publicKey, secretKey)
    }

    companion object {
        val LOGGER = logger()
    }
}
