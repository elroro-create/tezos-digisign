package com.sword.signature.daemon.job

import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Timer
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
class Metrics(
    val registry: MeterRegistry
) {

    fun getKOSignatureOperationCounter() = Counter.builder("number_ko_signature_operation").register(registry)
    fun getKOTransferOperationCounter() = Counter.builder("number_ko_transfer_operation").register(registry)
    fun getKORevealOperationCounter() = Counter.builder("number_ko_reveal_operation").register(registry)

    fun getOKSignatureOperationCounter() = Counter.builder("number_ok_signature_operation").register(registry)
    fun getOKTransferOperationCounter() = Counter.builder("number_ok_transfer_operation").register(registry)
    fun getOKRevealOperationCounter() = Counter.builder("number_ok_reveal_operation").register(registry)

    fun getSignatureOperationTimer() = Timer.builder("time_signature_operation").register(registry)
    fun getTransferOperationTimer() = Timer.builder("time_transfer_operation").register(registry)
    fun getRevealOperationTimer() = Timer.builder("time_reveal_operation").register(registry)

}
