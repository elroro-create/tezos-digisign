package com.sword.signature.daemon.job.validation

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.model.*
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.JobService
import com.sword.signature.business.service.dummyAdminAccount
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.daemon.job.Metrics
import com.sword.signature.daemon.logger
import com.sword.signature.daemon.sendPayload
import com.sword.signature.tezos.node.NodeConnector
import com.sword.signature.tezos.node.model.TzBlockResponse
import com.sword.signature.tezos.service.TezosReaderService
import org.springframework.messaging.MessageChannel
import java.time.Duration
import java.time.OffsetDateTime
import java.util.concurrent.TimeUnit

interface TezosOperationValidationJob {

    val jobService: JobService
    val accountService: AccountService
    val tezosReaderService: TezosReaderService
    val validationRetryMessageChannel: MessageChannel
    val anchoringMessageChannel: MessageChannel
    val nodeConnector: NodeConnector

    val minDepth: Long
    val validationTimeout: Duration

    val metrics: Metrics

    /**
     * Checks if a transaction is validated
     */
    suspend fun validate(payload: TezosOperationValidationJobPayload) {
        val requesterId = payload.requesterId
        val jobId = payload.jobId
        val transactionHash = payload.transactionHash
        val lastlyCheckedLevel = payload.lastlyCheckedLevel

        LOGGER.debug("Starting validation for job $jobId with transaction $transactionHash.")

        val requester = getAccount(requesterId)

        val job = jobService.findByIdJob(requester, jobId)
            ?: throw IllegalStateException("The job with id = $jobId should exist")

        // Check job status
        if (job.state != JobStateType.INJECTED) {
            LOGGER.info("Job '${job.id}' is '${job.state}' so it cannot be validated.")
            return
        }

        try {

            if (job.blockHash == null) {

                val headBlock = nodeConnector.getHeadBlock()
                val offsetLastPredToCheck = headBlock.header.level - lastlyCheckedLevel
                var transactionFound = false

                LOGGER.debug("Exploring all blocks from '${headBlock.hash}' to ${offsetLastPredToCheck}e predecessor.")
                // Explore each block from the head block to the lastly checked block
                for (i in 0..offsetLastPredToCheck) {
                    val operationHashes = nodeConnector.getBlockOperationHashes(
                        blockHash = headBlock.hash, predecessorOffset = i
                    ) ?: throw BlockShouldExistException()

                    if (operationHashes.contains(transactionHash)) {
                        transactionFound = true // Transaction FOUND in a BLOCK
                        val block = nodeConnector.getBlock(blockHash = headBlock.hash, predecessorOffset = i)
                            ?: throw BlockShouldExistException()
                        LOGGER.debug("Transaction found in block '${block.hash}'.")

                        checkDepth(block, transactionHash, job, requester, payload)
                        break
                    }
                }

                if (!transactionFound) { // Transaction not found in a block
                    val updatedPayload = payload.copy(lastlyCheckedLevel = headBlock.header.level)

                    // Update the lastlyChecked block level retry later
                    if (OffsetDateTime.now() > payload.injectionTime.plus(validationTimeout)) {
                        LOGGER.info(
                            "No confirmation found after ${validationTimeout.toMinutes()}m " +
                                    "for transaction $transactionHash (jobId=$jobId}). Anchoring will be retried..."
                        )

                        jobService.patchJob(
                            requester = requester,
                            jobId = payload.jobId,
                            patch = JobPatch(
                                state = JobStateType.INSERTED,
                                numberOfTry = 0
                            )
                        )

                        onValidationFailed(requester, updatedPayload)

                    } else {
                        LOGGER.info("No confirmation found for transaction $transactionHash (job=$jobId) yet.")

                        validationRetryMessageChannel.sendPayload(updatedPayload)
                    }
                }

            } else {
                val block = nodeConnector.getBlock(job.blockHash!!)
                    ?: throw BlockShouldExistException()

                checkDepth(block, transactionHash, job, requester, payload)
            }

        } catch (e: Exception) {
            LOGGER.error("There was an error while validating transaction : '$transactionHash', Retrying later.", e)
            validationRetryMessageChannel.sendPayload(payload)
        }
    }

    private suspend fun checkDepth(
        block: TzBlockResponse,
        transactionHash: String,
        job: TezosOperationJob,
        requester: Account,
        payload: TezosOperationValidationJobPayload
    ) {
        LOGGER.debug("Checking depth of block ${block.hash}.")
        val blockDepth = getDepth(block.header.level)

        if (blockDepth >= minDepth) {
            LOGGER.info("Transaction $transactionHash for job ${job.id} validated.")

            val timestamp =
                if (job is Job) {

                    tezosReaderService.getValueFromContractStorage(
                        job.contractAddress!!,
                        job.rootHash!!
                    )!!.timestamp

                } else {
                    block.header.timestamp
                }

            val updatedJob = jobService.patchJob(
                requester, job.id, JobPatch(
                    timestamp = timestamp,
                    state = JobStateType.VALIDATED,
                    blockHash = block.hash,
                    blockDepth = blockDepth
                )
            )

            // VALIDATED
            val jobDurationInSeconds =
                if (updatedJob.validatedDate != null) {
                    updatedJob.validatedDate!!.toEpochSecond() - updatedJob.createdDate.toEpochSecond()
                } else {
                    null
                }

            when (updatedJob) {
                is Job -> {
                    metrics.getOKSignatureOperationCounter().increment()
                    if (jobDurationInSeconds != null) {
                        metrics.getSignatureOperationTimer().record(jobDurationInSeconds, TimeUnit.SECONDS)
                    }
                }
                is RevealJob -> {
                    metrics.getOKRevealOperationCounter().increment()
                    if (jobDurationInSeconds != null) {
                        metrics.getRevealOperationTimer().record(jobDurationInSeconds, TimeUnit.SECONDS)
                    }
                }
                is TransferJob -> {
                    metrics.getOKTransferOperationCounter().increment()
                    if (jobDurationInSeconds != null) {
                        metrics.getTransferOperationTimer().record(jobDurationInSeconds, TimeUnit.SECONDS)
                    }
                }
            }

            onValidated(requester, payload)

        } else {
            LOGGER.info(
                "$blockDepth/$minDepth confirmations found " +
                        "for transaction $transactionHash (job=${job.id})."
            )

            jobService.patchJob(
                requester, job.id, JobPatch(
                    blockHash = block.hash,
                    blockDepth = blockDepth
                )
            )

            // FOUND BUT NOT ENOUGH CONFIRMATIONS
            validationRetryMessageChannel.sendPayload(payload)
        }
    }

    /**
     * Gets depth of the given level
     */
    private suspend fun getDepth(level: Long): Long {
        return nodeConnector.getHeadBlockLevel() - level
    }

    suspend fun getAccount(accountId: String): Account {
        return accountService.getAccount(dummyAdminAccount, accountId) ?: throw EntityNotFoundException(
            "account",
            accountId
        )
    }

    /**
     * When the transaction has been validated (it has been anchored in a block that is now at minDepth)
     */
    suspend fun onValidated(requester: Account, payload: TezosOperationValidationJobPayload)

    /**
     * The transaction has not been anchored until the timeout
     */
    suspend fun onValidationFailed(requester: Account, payload: TezosOperationValidationJobPayload)

    companion object {
        class BlockShouldExistException : IllegalStateException("Block should exist")

        val LOGGER = logger()
    }
}
