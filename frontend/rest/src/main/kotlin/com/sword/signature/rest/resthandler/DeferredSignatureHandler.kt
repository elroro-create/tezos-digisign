package com.sword.signature.rest.resthandler

import com.sword.signature.api.sign.DeferredSignatureFile
import com.sword.signature.api.sign.DeferredSignatureRequest
import com.sword.signature.api.sign.DeferredSignatureResponse
import com.sword.signature.business.model.DeferredSignatureFileCreate
import com.sword.signature.business.service.DeferredSignatureService
import com.sword.signature.common.criteria.DeferredSignatureFileCriteria
import com.sword.signature.rest.data.pagedSorted
import com.sword.signature.webcore.authentication.CustomUserDetails
import com.sword.signature.webcore.mapper.toBusiness
import com.sword.signature.webcore.mapper.toWeb
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("\${api.base-path:/api}")
class DeferredSignatureHandler(
    val deferredSignatureService: DeferredSignatureService
) {

    @Operation(security = [SecurityRequirement(name = "bearer-key")])
    @RequestMapping(
        value = ["/deferred/files"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    suspend fun getFiles(
        @AuthenticationPrincipal user: CustomUserDetails,
        @RequestParam(value = "id", required = false) id: String?,
        @RequestParam(value = "name", required = false) name: String?,
        @RequestParam(value = "hash", required = false) hash: String?,
        @RequestParam(value = "accountId", required = false) accountId: String?,
        @RequestParam(value = "sort", required = false) sort: List<String>?,
        @RequestParam(value = "page", required = false) page: Int?,
        @RequestParam(value = "size", required = false) size: Int?
    ): Flow<DeferredSignatureFile> {

        val paged = pagedSorted(page, size, sort)

        val criteria = DeferredSignatureFileCriteria(
            id = id,
            accountId = accountId,
            hash = hash,
            name = name
        )

        return deferredSignatureService.getFiles(user.account, criteria, paged).map { it.toWeb() }
    }

    @Operation(security = [SecurityRequirement(name = "bearer-key")])
    @RequestMapping(
        value = ["/deferred/files-count"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    suspend fun fileCount(
        @AuthenticationPrincipal user: CustomUserDetails,
        @RequestParam(value = "id", required = false) id: String?,
        @RequestParam(value = "name", required = false) name: String?,
        @RequestParam(value = "hash", required = false) hash: String?,
        @RequestParam(value = "accountId", required = false) accountId: String?,
    ): Long {
        val criteria = DeferredSignatureFileCriteria(
            id = id,
            name = name,
            hash = hash,
            accountId = accountId,
        )

        return deferredSignatureService.countFiles(requester = user.account, criteria = criteria)
    }

    @Operation(security = [SecurityRequirement(name = "bearer-key")])
    @RequestMapping(
        value = ["/deferred/sign"],
        produces = ["application/json"],
        consumes = ["application/json"],
        method = [RequestMethod.POST]
    )
    suspend fun deferredSignature(
        @AuthenticationPrincipal user: CustomUserDetails,
        @RequestBody request: DeferredSignatureRequest
    ): DeferredSignatureResponse {

        val files =
            request.files.map {
                deferredSignatureService.deferredSignature(
                    requester = user.account,
                    signatureFileDetails = DeferredSignatureFileCreate(
                        userId = user.account.id,
                        hash = it.hash,
                        metadata = it.metadata.toBusiness()
                    )
                ).metadata.toWeb()
            }

        return DeferredSignatureResponse(
            files = files
        )
    }

}
