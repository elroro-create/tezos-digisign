package com.sword.signature.rest.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.web.reactive.config.CorsRegistry
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.config.WebFluxConfigurer

@Configuration
@Profile("developer")
class DeveloperWebFluxConfigurer: WebFluxConfigurer {

    override fun addCorsMappings(corsRegistry: CorsRegistry) {
        corsRegistry.addMapping("/api/**")
            .allowCredentials( true)
            .allowedOriginPatterns("*")
            .allowedHeaders("*")
            .allowedMethods("*")
    }

}
