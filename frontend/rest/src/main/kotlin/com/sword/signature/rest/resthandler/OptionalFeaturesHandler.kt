package com.sword.signature.rest.resthandler

import com.sword.signature.business.configuration.OptionalFeaturesConfig
import com.sword.signature.rest.toWeb
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("\${api.base-path:/api}")
class OptionalFeaturesHandler(val optionalFeaturesConfig: OptionalFeaturesConfig) {

    @RequestMapping(
        value = ["/optionalFeatures"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    suspend fun getJsClientEnvironment() = optionalFeaturesConfig.toWeb()

}
