export interface PaginationOption {
    page: number,
    itemsPerPage: number,
    sortBy: Array<string>,
    sortDesc: Array<boolean>,
}

export interface FilterOption {
    flowName: string | undefined,
    id: string | undefined,
    dates: Array<string | undefined>,
    channelName: string | undefined,
}

export interface Metadata {
    key: string,
    value: string
}

export interface FileSign {
    name: string,
    size: number,
    hash: string,
    metadata: Array<Metadata>
}

export interface FileFilterOption {
    id?: string,
    name?: string,
    hash?: string,
    jobId?: string,
    accountId?: string,
    dates: Array<string | undefined>,
}

export interface DeferredFileFilterOption {
    id?: string,
    name?: string,
    hash?: string,
    accountId?: string,
}
