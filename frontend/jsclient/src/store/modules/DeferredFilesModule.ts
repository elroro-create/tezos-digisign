import {DeferredFileFilterOption, PaginationOption} from "@/store/types"
import {DeferredFile, DeferredFileCriteria} from "@/api/types"
import {Action, Module, Mutation, VuexModule} from "vuex-class-modules"
import AccountsModule from "@/store/modules/AccountsModule"
import deepcopy from "ts-deepcopy"
import {deferredSignatureApi} from "@/api/deferredSignatureApi"

@Module
export default class DeferredFilesModule extends VuexModule {

    private accountsModule: AccountsModule

    private isLoading: boolean = false
    private fileList: Array<DeferredFile> = []
    private fileCount: number = 0
    private paginationOption: PaginationOption = {
        page: 1,
        itemsPerPage: 10,
        sortBy: [],
        sortDesc: [],
    }
    private filter: DeferredFileFilterOption = {
        accountId: undefined,
        name: undefined,
        id: undefined,
        hash: undefined,
    }

    constructor(options: any) {
        super(options)
        this.accountsModule = options.accountsModule
    }

    @Action
    public async loadFiles() {
        this.setLoading(true)
        this.filterUpdate()
        const sorts: Array<string> = []
        for (let i = 0; i < this.paginationOption.sortBy.length; ++i) {
            sorts.push(this.paginationOption.sortBy[i] + ":" + ((this.paginationOption.sortDesc[i]) ? "desc" : "asc"))
        }

        const criteria: DeferredFileCriteria = {
            accountId: this.accountsModule.meAccount?.id,
            id: this.filter.id,
            hash: this.filter.hash,
            name: this.filter.name,
            sort: sorts,
            page: this.paginationOption.page - 1,
            size: this.paginationOption.itemsPerPage,
        }

        await deferredSignatureApi.list(criteria).then((response: Array<DeferredFile>) => {
            this.setFiles(response)
        })
        await deferredSignatureApi.count(criteria).then((response: number) => {
            this.setFileCount(response)
        })
        this.setLoading(false)
    }

    public get getLoading(): boolean {
        return this.isLoading
    }

    @Mutation
    public setFiles(jobs: Array<DeferredFile>) {
        this.fileList = jobs
    }

    public get getFiles(): Array<DeferredFile> {
        return this.fileList
    }

    @Mutation
    public setFileCount(count: number) {
        this.fileCount = count
    }

    public get getFileCount(): number {
        return this.fileCount
    }

    @Mutation
    public setPagination(pg: PaginationOption) {
        this.paginationOption = deepcopy<PaginationOption>(pg)
    }

    public getPagination() {
        return deepcopy<PaginationOption>(this.paginationOption)
    }

    @Mutation
    public setFilter(f: DeferredFileFilterOption) {
        this.filter = deepcopy<DeferredFileFilterOption>(f)
    }

    public getFilter() {
        return deepcopy<DeferredFileFilterOption>(this.filter)
    }

    @Mutation
    private setLoading(isLoading: boolean) {
        this.isLoading = isLoading
    }

    @Mutation
    private filterUpdate() {
        if (this.filter.name === "") {
            this.filter.name = undefined
        }
        if (this.filter.id === "") {
            this.filter.id = undefined
        }
    }
}
