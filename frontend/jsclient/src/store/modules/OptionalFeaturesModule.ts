import {Action, Module, Mutation, VuexModule} from "vuex-class-modules"
import {OptionalFeatures} from "@/api/types"
import {optionalFeaturesApi} from "@/api/optionalFeaturesApi"


@Module
export default class OptionalFeaturesModule extends VuexModule {

    public optionalFeatures: OptionalFeatures | undefined = undefined

    @Action
    public async loadOptionalFeatures() {
        await optionalFeaturesApi.optionalFeatures().then((response: OptionalFeatures) => {
            this.setOptionalFeatures(response)
        })
    }

    @Mutation
    public setOptionalFeatures(optionalFeatures: OptionalFeatures) {
        this.optionalFeatures = optionalFeatures
    }

}
