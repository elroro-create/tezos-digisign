import {OptionalFeatures} from "@/api/types"
import {AxiosRequestConfig, AxiosResponse} from "axios"

import {Api} from "@/api/api"
import {apiConfig} from "@/api/api.config"

export const API_GET_ENVIRONMENT = "/optionalFeatures"

export class OptionalFeaturesApi extends Api {
    public constructor(config: AxiosRequestConfig) {
        super(config)
        this.optionalFeatures = this.optionalFeatures.bind(this)
    }

    public optionalFeatures(): Promise<OptionalFeatures> {
        return this.get<OptionalFeatures>(API_GET_ENVIRONMENT)
            .then((response: AxiosResponse<OptionalFeatures>) => {
                return response.data
            })
    }

}

export const optionalFeaturesApi = new OptionalFeaturesApi(apiConfig)
