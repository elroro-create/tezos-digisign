# Decrypt Tezos encrypted secret key

## Requirements

* Install pip dependencies
```
pip3 install -r requirements.txt
```

## Usage

* Call the script with the encrypted secret key and the encryption password
```
python3 decrypt_key.py encrypted_secret_key password
```